'use strict';

const assert = require('assert');

const {
  min, max, abs, floor,
  ceil, round, sqrt, sin, cos,
  atan, atan2, clz32,
  log2,
} = Math;

const threshold = 1e-3;

const poly = (n, e) => {
  const arr = O.ca(n, i => (n - i) ** e);
  return normalize(arr);
};

const exp = (n, b) => {
  const arr = O.ca(n, i => b ** (n - i));
  return normalize(arr);
};

const exp1 = (n=1e7, fac=1/5) => {
  const d = O.obj();
  
  const put = function*(i, n){
    if(!O.has(d, i)) d[i] = 0;
    
    const a = n * fac;
    const b = (n - a) / 2;
    
    d[i] += a;
    
    if(n < 1) return;
    
    yield [put, i - 1, b];
    yield [put, i + 1, b];
  };
  
  O.rec(put, 0, n);
  
  const inds = O.keys(d).map(a => a | 0);
  const indMin = inds.reduce((a, b) => min(a, b), O.N);
  const indsNum = inds.length;
  
  const arr = O.ca(indsNum, i => {
    return d[indMin + i];
  });
  
  return normalize(arr, 0);
};

const normalize = (arr, symm=1) => {
  if(symm)
    arr = [...arr.slice(1).reverse(), ...arr];
  
  const arrSum = arr.reduce((a, b) => a + b, 0);
  
  arr = arr.map(a => {
    return a / arrSum;
  });
  
  while(arr.length !== 0 && arr[0] < threshold)
    arr.shift();
  
  while(arr.length !== 0 && O.last(arr) < threshold)
    arr.pop();
  
  if(arr.length === 0)
    return arr;

  const len = arr.length;
  assert(len & 1);
  
  return arr.slice(len >> 1);
};

module.exports = {
  poly,
  exp,
  exp1,
};