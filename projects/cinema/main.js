"use strict"

const fs = require("fs")
const path = require("path")
const assert = require("assert")
const fw_reg = require("/../Other/Cinema/regex")

const {min, max} = Math;

const CHECK_REG = 1
const DRAW_BLOCKS = 0

const movie = O.urlParam('movie');
if(movie === null) return;

await O.addStyle('style.css');

const volumeFactor = .8
const fontFamily = 'Arial'
const fontSize = 50
const textSpacing = 2
const boldText = 1
const yOffset = 100
const dpos = 5

const cwd = __dirname;

let subs = null;
let subsNum = 0;

let video;
let dur;
let subTimeOffset;

let w, h;
let wh, hh;
let overlay, g;

let subsEnabled = 1;
let subIndex = null;
let subIndexN = null;
let volumeSet = 0;
let gainNode = null;
let volumeInfo = 0;
let timeOffset = null;
let muted = 0

const main = async () => {
  document.title = "\u034f"
  
  let videoFile = movie;
  let subFile = get_sub_file_pth(videoFile);
  let timeOffsetStr = O.urlParam('t', '');
  let subOffsetStr = O.urlParam('s', '0.5');
  
  if(subFile === '') subFile = null;
  if(subOffsetStr === '') subOffsetStr = '0';
  if(timeOffsetStr === '') timeOffsetStr = null;
  
  videoFile = videoFile.replace(/\\/g, '/');
  subFile = subFile !== null ? subFile.replace(/\\/g, '/') : null;
  
  video = O.ce(O.body, 'video');
  subTimeOffset = Number(subOffsetStr.replace(/\:/g, '.'));
  timeOffset = timeOffsetStr;

  const source = O.ce(video, 'source');
  source.src = O.urlTime(mkPth(videoFile));
  await new Promise(res => O.ael(video, 'loadeddata', res));

  subs = subFile !== null ? await loadSubs(mkPth(subFile)) : null;
  if(subs !== null) subsNum = subs.length;

  dur = video.duration;
  setTimeOffset();
  createOverlay();

  aels();
  render();
};

const mkPth = str => {
  return `/${str}`.replace(/ /g, '%20');
};

const loadSubs = async subFile => {
  const res = await O.rfs('/' + subFile, 1);
  if(res === null) return null;
  
  let str = O.lf(res).trim();
  
  if(str[0] === '{'){
    return O.sanl(str).map(str => {
      const match = str.match(/^\{(\d+)\}\{(\d+)\}(.*)$/);
      assert(match !== null);
    
      const [from, to] = O.ca(2, i => Number(match[i + 1]) / (24000 / 1001));
      const text = match[3].replace(/\|/g, '\n');
    
      return [from, to, text];
    });
  }
  
  const subs = [];
  let subsNum = 0;
  
  str = str.replace(/(\S)\n(\d+)\n(\S)/g, (a, b, c, d) => `${b}\n\n${c}\n${d}`);

  while(1){
    str = str.trimLeft();
    if(str.length === 0) break;

    const match = str.match(/^((?:\d+\n)?)\n?([^\n]+)(?:\n|$)(.*?)(?:(?<=\n)\n|$)/s);
    assert(match !== null);

    str = str.slice(match[0].length);

    // log(O.sf(match[0]));
    // log(O.sf(str.slice(0, 100)));
    // log()
    // if(prompt())z

    // const index = match[1].trim() || null;
    // assert(index === String(subsNum + 1));

    const interval = match[2];
    const parts = interval.split(' --> ');
    assert(parts.length === 2);

    const timeOffsets = parts.map(str => {
      const match = str.match(/^(\d{2})\:(\d{2})\:(\d{2})[\,\.](\d{3})(?: .*)?$/);
      assert(match !== null);

      const ns = match.slice(1).map(a => a | 0).reverse();

      return 1e-3 * ns[0] + ns[1] + 60 * (ns[2] + 60 * ns[3]);
    });

    let [of1, of2] = O.sortAsc(timeOffsets);
    assert(of2 >= of1);

    let text = match[3].trim()
      .replace(/\<[^\>]*\>/g, '')
      .replace(/(?:\-\s*)?\([^\)]*\)/g, ' ')
      .replace(/(?:\-\s*)?\[[^\]]*\]/g, ' ')
      .replace(/♪.*/g, ' ')
      .replace(/ {2,}/g, ' ')
      .replace(/\{[^\}]*\}/g, ' ')
      // .replace(/([^\.])\.$/g, (a, b) => b)
    
    text = O.sanl(text).map(a => a.trim()).
      filter(a => a !== '').join('\n');
    
    if(text === '') continue;

    if(subsNum !== 0){
      let prev = O.last(subs)
      let [of1_prev, of2_prev, text_prev] = prev
      
      if(of1 < of2_prev){
        assert(of1_prev < of2)
        prev[1] = of1
        of2_prev = of1
      }
      
      if(!(of2_prev - of1 < 1)){
        // Duplicate entry
        
        assert(of1 >= of1_prev)
        assert(of2 >= of2_prev)
        prev[1] = of2
        
        if(text_prev === text) continue
        
        prev[2] += `\n${text}`
      }else{
        of1 = max(of2_prev, of1)
      }
    }

    subs.push([of1, of2, text]);
    subsNum++;
  }
  
  const len = subs.length
  
  // for(let i = 0; i !== len; i++){
  //   const e = subs[i]
  //   let s = e[2]
  // 
  //   if(fw_reg.test(s))
  //     s = ""
  // 
  //   if(i !== len - 1){
  //     const e1 = subs[i + 1]
  //     if(fw_reg.test(e1[2]))
  //       s = `${s}\n#`.trim()
  //   }
  // 
  //   e[2] = s
  // }

  return subs;
};

const setTimeOffset = () => {
  if(timeOffset == null) return;
  
  const t = timeOffset.split(':').reduce((a, b) => a * 60 + Number(b), 0);
  video.currentTime = t;
};

const createOverlay = () => {
  overlay = O.ce(O.doc.documentElement, 'canvas');
  g = overlay.getContext('2d');

  onResize();
};

const aels = () => {
  O.ael('resize', onResize);
  O.ael('keydown', onKeyDown);
  
  let t = 0
  
  const f = () => {
    video.blur()
    t = video.currentTime
    O.raf(f)
  }
  
  f()

  O.ael(video, 'keydown', evt => {
    const {code} = evt;
    
    if(code === "Space"){
      if(video.paused) video.play()
      else video.pause()
      evt.stopPropagation()
      return
    }
    
    if(code.startsWith('Arrow')){
      video.currentTime = t
      evt.stopPropagation()
      return
    }
  });
};

const onResize = evt => {
  w = O.iw;
  h = O.ih;

  wh = w / 2;
  hh = h / 2;

  overlay.width = w;
  overlay.height = h;

  g.lineWidth = 4;
  g.lineJoin = 'round';

  g.textBaseline = 'bottom';
  g.textAlign = 'center';

  const boldStr = boldText ? `bold ` : '';
  g.font = `${boldStr} ${fontSize}px ${fontFamily}`;

  g.globalCompositeOperation = 'destination-over';

  subIndex = null;
  
  g.clearRect(0, 0, w, h);
  drawBlocks();
};

const onKeyDown = async evt => {
  const {ctrlKey, shiftKey, altKey, code} = evt;
  const flags = (ctrlKey << 2) | (shiftKey << 1) | altKey;
  
  if(flags === 4){
    // if(code === 'ArrowRight'){
    //   O.pd(evt);
    //   video.currentTime = min(video.currentTime + 19 * 5, dur);
    //   subIndexN = null
    //   return;
    // }
    
    return
  }

  if(flags === 0){
    if(code === 'Space'){
      O.pd(evt);
      
      if(!video.paused){
        video.pause();
        return;
      }

      if(!volumeSet) initVolume();
      video.play();
      
      return;
    }

    if(code === 'ArrowLeft'){
      O.pd(evt);
      video.currentTime = max(video.currentTime - dpos, 0);
      subIndexN = null
      return;
    }

    if(code === 'ArrowRight'){
      O.pd(evt);
      video.currentTime = min(video.currentTime + dpos, dur);
      subIndexN = null
      return;
    }
    
    if(code === 'ArrowUp'){
      volumeInfo++;
      updateVolume();
      return;
    }
    
    if(code === 'ArrowDown'){
      volumeInfo--;
      updateVolume();
      return;
    }
    
    if(code === 'KeyQ'){
      O.pd(evt);
      video.controls ^= 1;
      return;
    }

    if(code === 'KeyM'){
      O.pd(evt);
      muted ^= 1
      if(subs === null) video.muted = muted
      return;
    }

    if(code === 'KeyS'){
      if(subs === null) return;
      O.pd(evt);
      subsEnabled ^= 1;
      return;
    }

    return;
  }
};

const initVolume = () => {
  const ctx = new AudioContext();
  const src = ctx.createMediaElementSource(video);
  
  gainNode = ctx.createGain();
  src.connect(gainNode);
  gainNode.connect(ctx.destination);
  updateVolume();
  
  // gainNode = {gain: {}};
  
  volumeSet = 1;
};

const updateVolume = () => {
  gainNode.gain.value = calcVolumeFromInfo(volumeInfo);
};

const calcVolumeFromInfo = info => {
  if(info >= 0) return 1 + info * 0.3;
  return (1 / volumeFactor) ** info;
};

const render = () => {
  const t = video.currentTime;
  
  if(subs !== null){
    const index = calcSubIndex(t)
    const subIndexNew = subsEnabled ? index : null
    
    if(index !== null) subIndexN = index
    
    let ok = 1
    
    if(CHECK_REG && subIndexN !== null){
      const start = max(0, subIndexN)
      const end = min(subs.length - 1, subIndexN + 1)
      
      for(let i = start; i <= end; i++){
        const s = subs[i][2]
        
        if(fw_reg.test(s)){
          ok = 0
          break
        }
      }
    }
    
    video.muted = muted || !ok
    
    if(subIndexNew !== subIndex){
      subIndex = subIndexNew
      draw_sub_index(subIndex)
    }
  }

  O.raf(render);
};

const drawBlocks = () => {
  if(!DRAW_BLOCKS) return;
  
  g.fillStyle = '#808080';
  
  // const x = 0
  // const y = 0
  // const ws = 287
  // const hs = 224
  // const ws2 = ws + 2;
  // const hs2 = hs + 2;
  // 
  // g.fillRect(0, 0, ws, hs);
  // g.fillRect(w - ws, 0, ws2, hs);
  // g.fillRect(0, h - hs, ws, hs2);
  // g.fillRect(w - ws, h - hs, ws2, hs2);
  
  const ws = 790 * 2
  const hs = 186
  
  g.fillRect((w - ws) / 2, h - hs, ws, hs + 2);
};

const draw_sub_index = index => {
  g.clearRect(0, 0, w, h);
  drawBlocks();

  if(index !== null){
    const s = subs[index][2]
    if(CHECK_REG && fw_reg.test(s)) return
    
    drawSubs(s)
  }
}

const drawSubs = str => {
  const lines = O.sanl(str);
  let y = h - yOffset;

  for(let i = lines.length - 1; i !== -1; i--){
    const line = lines[i];

    g.fillStyle = '#ff8';
    g.fillText(line, wh, y);
    g.strokeText(line, wh, y);

    y -= fontSize + textSpacing;
  }
};

const calcSubIndex = t => {
  // if(!subsEnabled)
  //   return null;

  t -= subTimeOffset;

  const index = Number(O.bisect(i => {
    i = Number(i);
    if(i >= subsNum) return 1;
    return subs[i][0] > t;
  })) - 1;

  if(index === -1)
    return null;

  assert(index >= 0);
  assert(index < subsNum);

  const [t1, t2] = subs[index];

  if(t >= t1 && t <= t2)
    return index;

  return null;
};

const get_sub_file_pth = pth => {
  pth = pth.split('/');
  
  let base = pth.pop().split('.');
  assert(base.length >= 2);
  
  let name = base.slice(0, -1).join('.');
  if(name.endsWith('~')) name = name.slice(0, -1);
  
  pth.push(`${name}.srt`);
  
  return pth.join('/');
};

main();