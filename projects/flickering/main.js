'use strict';

const assert = require('assert');
const dataFile = require('./data');

const w = 464
const h = 512

const {g} = O.ceCanvas();
const {canvas} = g;

const imgd = g.createImageData(w, h);
const {data} = imgd;

const bufSize = w * h;
const sample = new Uint8Array(bufSize);
const state = new Uint8Array(bufSize);

const main = () => {
  let s = dataFile.replace(/\s/g, '');
  
  iter((x, y, i) => {
    sample[i] = O.cc(s[i]) - 0x21;
    state[i] = O.rand(64);
  });
  
  data.fill(255);
  
  aels();
  onResize();
  render();
};

const iter = f => {
  for(let y = 0, i = 0; y !== h; y++)
    for(let x = 0; x !== w; x++, i++)
      f(x, y, i);
};

const aels = () => {
  O.ael('resize', onResize);
};

const onResize = () => {
  const {iw, ih} = O;
  
  canvas.width = iw;
  canvas.height = ih;
};

const update = () => {
  iter((x, y, i) => {
    let j = i << 2;
    
    state[i] = state[i] + O.rand(0, 2) & 63;
    
    if(state[i] < sample[i]){
      if(data[j] === 255) return;
      data[j] = data[j + 1] = data[j + 2] = 255;
      return;
    }
    
    if(data[j] === 0) return;
    data[j] = data[j + 1] = data[j + 2] = 0;
  });
};

const render = () => {
  const {iw, ih} = O;
  
  update();
  
  g.fillStyle = 'darkgray';
  g.fillRect(0, 0, iw, ih);
  
  g.putImageData(imgd, iw - w >> 1, ih - h >> 1);
  
  O.raf(render);
};

main();