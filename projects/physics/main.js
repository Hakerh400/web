"use strict"

const fs = require("fs")
const path = require("path")
const assert = require("assert")

const {
  min, max, abs, floor,
  ceil, round, sqrt, sin, cos,
  atan, atan2, clz32,
  log2,
} = Math

const {pi, pih, pi2} = O

const tick_interval = 1
const tile_size = 50
const dx_max = 1
const gravity = 0.1
const collision_friction = 0.01

const cols = {
  bg: "#888",
}

let canvas, g
let iw, ih
let iwh, ihh
let sim

const main = () => {
  clear_title()
  
  iw = O.iw
  ih = O.ih
  iwh = iw / 2
  ihh = ih / 2
  
  let arena = new Cuboid(Vector.ZERO, new Vector(iw, ih))
  
  g = O.ceCanvas().g
  canvas = g.canvas
  
  /////
  
  sim = new Simulator()
  sim.tile_size = tile_size
  sim.dx_max = dx_max
  sim.collision_friction = collision_friction
  sim.gravity = gravity ? Vector.down(gravity) : Vector.ZERO
  
  let bonds = 0
  
  /////
  
  if(1){
    let r = 25
    let d = r * 2
    let m = 1e15
    
    for(let x = r; x <= iw; x += d){
      let p = new Particle(r - 1, m, new Vector(x, r))
      p.static = 1
      p.col = new Color(1, 1, 1)
      sim.add_particle(p)
      
      p = new Particle(r - 1, m, new Vector(x, ih - O.mod(ih, d) - r))
      p.static = 1
      p.col = new Color(1, 1, 1)
      sim.add_particle(p)
    }
    
    for(let y = r + d; y <= ih - d; y += d){
      let p = new Particle(r - 1, m, new Vector(r, y))
      p.static = 1
      p.col = new Color(1, 1, 1)
      sim.add_particle(p)
      
      p = new Particle(r - 1, m, new Vector(iw - O.mod(iw, d) - r, y))
      p.static = 1
      p.col = new Color(1, 1, 1)
      sim.add_particle(p)
    }
  }
  
  // let pa, pb
  
  // for(let i = 0; i !== 5; i++){
  //   let p1 = new Particle(30, 1, arena.middle().add(Vector.left(i * 61)))
  //   p1.vel = Vector.right().add(Vector.up()).len(1)
  //   p1.col = new Color(0, 1, 0)
  //   sim.add_particle(p1)
  //   
  //   let p2 = new Particle(30, 1, p1.pos.add(p1.vel.norm().mul(160)).add(Vector.right(70)).add(Vector.left(i * 45)))
  //   p2.col = new Color(1, 1, 0)
  //   sim.add_particle(p2)
  //   
  //   // if(i === 0) pa = p1
  //   // if(i === 4) pb = p2
  // }
  // 
  // let p3 = new Particle(40, 1, new Vector(200, 100))
  // p3.mass = 10
  // p3.vel = Vector.right(0)
  // p3.col = new Color(1, 0, 1)
  // sim.add_particle(p3)
  
  {
    let r = 25
    let d = r * 2
    let m = 1e15
    
    let p
    
    for(let j = 0; j !== 2; j++){
      for(let i = 0; i !== 3; i++){
        if(i === 1 && j === 0) continue
        
        p = new Particle(r - 1, m, new Vector(d * (1.5 + i), d * (3.5 + j)))
        p.static = 1
        p.col = new Color(1, 1, 1)
        sim.add_particle(p)
        
        if(i === 2 && j === 0)
          p.pos = p.pos.add(Vector.left(0))
      }
    }
    
    p = new Particle(r - 1, 1, new Vector(d * (1.5 + 0.5), d * (3.5 - 1)))
    p.col = new Color(0, 1, 0)
    sim.add_particle(p)
  }
  
  sim.update_grid()
  sim.pause()
  
  O.ael("keydown", evt => {
    let {code} = evt
    
    if(code === "Space"){
      sim.paused ^= 1
      return
    }
    
    if(code === "Enter"){
      bonds ^= 1
      return
    }
    
    if(code === "KeyG"){
      sim.gravity = sim.gravity === Vector.ZERO ?
        Vector.down(0.1) : Vector.ZERO
      return
    }
  })
  
  /////
  
  // log(O.fst(sim.particles))
  
  const tick = () => {
    // p.pos = cur
    // sim.update_grid()
    
    if(bonds){
      let ps = [...sim.particles].filter(a => !a.static)
      
      for(let i = 0; i !== ps.length; i++){
        let j = (i + 1) % ps.length
        let pa = ps[i]
        let pb = ps[j]
        let angle = pa.pos.angle_to(pb.pos)
        let d = pa.rad + pb.rad
        let n = (pa.pos.dist(pb.pos) - d) / 1000
        let v = Vector.polar(n, angle)
        pa.vel = pa.vel.add(v)
        pb.vel = pb.vel.sub(v)
      }
    }
    
    sim.tick(tick_interval)
    render()
    O.raf(() => {
      setTimeout(tick)
    })
  }
  
  const render = () => {
    let {tile_size, grid} = sim
    
    g.fillStyle = cols.bg
    g.fillRect(0, 0, iw, ih)
    
    g.save()
    g.translate(-0.5, -0.5)
    
    if(0){
      for(let yn = 0;; yn++){
        let y = yn * tile_size
        if(y >= ih) break
      
        for(let xn = 0;; xn++){
          let x = xn * tile_size
          if(x >= iw) break
      
          let tile = grid.get(new Vector(xn, yn))
          g.fillStyle = tile === null ? cols.bg :
            tile.size === 1 ? "white" :
            tile.size === 2 ? "orange" :
            "orange"
      
          g.beginPath()
          g.rect(x, y, tile_size, tile_size)
          g.fill()
          g.stroke()
        }
      }
    }
    
    // let cm = Vector.ZERO
    // let m = 0
    
    for(let p of sim.particles){
      let {pos, vel, rad, mass, col} = p
      let {x, y} = pos
      
      g.fillStyle = col.toString()
      g.beginPath()
      g.arc(x, y, rad, 0, pi2)
      g.fill()
      g.stroke()
      
      // let angle = p.vel.angle()
      // g.beginPath()
      // g.moveTo(x, y)
      // g.lineTo(x + rad * cos(angle), y + rad * sin(angle))
      // g.stroke()
      
      // cm = cm.add(pos.mul(mass))
      // m += mass
    }
    
    if(bonds){
      let ps = [...sim.particles].filter(a => !a.static)
      
      for(let i = 0; i !== ps.length; i++){
        let j = (i + 1) % ps.length
        let pa = ps[i]
        let pb = ps[j]
        g.beginPath()
        g.moveTo(pa.pos.x, pa.pos.y)
        g.lineTo(pb.pos.x, pb.pos.y)
        g.stroke()
      }
    }
    
    // cm = cm.div(m)
    // 
    // g.fillStyle = "#f00"
    // g.beginPath()
    // g.arc(cm.x, cm.y, 4, 0, pi2)
    // g.fill()
    // g.stroke()
    
    g.restore()
  }
  
  tick()
}

const clear_title = () => {
  document.title = ""
}

class Simulator {
  paused = 0
  tile_size = 1
  dx_max = 1
  collision_friction = 0
  gravity = Vector.ZERO
  
  particles = new Set()
  grid = new VectorMap()
  
  add_particle(p){
    let {particles} = this
    
    assert(!particles.has(p))
    particles.add(p)
  }
  
  *iter_particle_vecs(p){
    let {tile_size, dx_max} = this
    let cuboid = p.get_bounding_cuboid(dx_max * 2)
    
    yield* cuboid.iter_sample_vecs(tile_size)
  }
  
  *iter_particle_tiles(p){
    let {grid} = this
    
    for(let v of this.iter_particle_vecs(p))
      yield grid.get(v)
  }
  
  calc_grid(){
    let {tile_size, particles} = this
    let grid = new VectorMap()
    
    for(let p of particles){
      for(let v of this.iter_particle_vecs(p)){
        if(!grid.has(v)){
          let tile = new Set()
          
          tile.add(p)
          grid.insert(v, tile)
          
          continue
        }
        
        grid.get(v).add(p)
      }
    }
    
    return grid
  }
  
  update_grid(){
    this.grid = this.calc_grid()
  }
  
  pause(){
    this.paused = 1
  }
  
  tick_aux(t){
    if(this.paused) return t
    
    let {
      particles, collision_friction,
      gravity,
    } = this
    
    // gravity = Vector.ZERO
    
    // for(let p1 of particles){
    //   let {id: id1, pos: pos1, rad: rad1} = p1
    //   
    //   for(let tile of this.iter_particle_tiles(p1)){
    //     for(let p2 of tile){
    //       if(id1 >= p2.id) continue
    //       if(p1.collisions.has(p2)) continue
    //       
    //       let {pos: pos2, rad: rad2} = p2
    //       let rad = rad1 + rad2
    //       let dist = pos1.dist(pos2)
    //       
    //       if(dist < rad){
    //         // debugger
    //         log("OVERLAP!!!", rad1 + rad2, dist)
    //         log(t)
    //         this.pause()
    //         return t
    //         // p1.col = new Color(1, 0, 0)
    //         // p2.col = new Color(1, 0, 0)
    //         // this.pause()
    //         // return t
    //       }
    //     }
    //   }
    // }
    
    for(let p of particles){
      let {pos, vel} = p
      
      // vel = vel.mul(0.99)
      // p.vel = vel
      
      p.pos1 = pos.add(vel.mul(t))
    }
    
    let t0 = t
    let collision = null
    
    // if(t === 1){
    //   t0 = t / 2
    //   collision = [...particles].reverse().slice(1, 3)
    // }else
    for(let p1 of particles){
      let {id: id1, pos: pos1, vel: vel1, rad: rad1} = p1
      
      for(let tile of this.iter_particle_tiles(p1)){
        for(let p2 of tile){
          if(id1 >= p2.id) continue
          if(p1.collisions.has(p2)) continue
          
          let {pos: pos2, vel: vel2, rad: rad2} = p2
          let rad = rad1 + rad2
          
          // if(pos1.dist(pos2) > rad) continue
          
          let d1 = p1.pos.sub(p2.pos)
          let d2 = p1.pos1.sub(p2.pos1)
          let {x: x1, y: y1} = d1
          let {x: x2, y: y2} = d2
          let dx = x1 - x2
          let dy = y1 - y2
          
          let a = dx * dx + dy * dy
          let b = 2 * (dx * x2 + dy * y2)
          let c = x2 * x2 + y2 * y2 - rad * rad
          
          let k
          
          if(a === 0){
            if(b === 0) continue
            
            k = -b / c
          }else{
            let a2 = a * 2
            let r12 = b * b - 4 * a * c
            if(r12 < 0) continue
            
            let r1 = sqrt(r12)
            let k1 = 1 - (-b + r1) / a2
            let k2 = 1 - (-b - r1) / a2
            
            let kd1 = abs(k1 - 0.5)
            let kd2 = abs(k2 - 0.5)
            
            k = kd1 < kd2 ? k1 : k2
          }
          
          if(k > 1) continue
          
          if(k < 0){
            if(p1.pos1.dist(p2.pos1) > rad) continue
            k = 0
          }
          
          // let f = pos1.angle_to(pos2)
          // let v1n = vel1.project(f)
          // let v2n = vel2.project(f)
          // if(v1n.len() + v2n.len() > v1n.add(v2n).len()){
          //   // log(v1n.len() + v2n.len())
          //   // log(v1n.add(v2n).len())
          //   // p1.col = new Color(1, 0, 0)
          //   // p2.col = new Color(1, 0, 0)
          //   // this.pause()
          //   // return t
          //   continue
          // }
          
          // k *= 0.5
          
          // if(k < 0 || k > 1) continue
          
          // if(k < 0) k = 0
          // if(!(k >= 0 && k <= 1)) continue
          // k = O.bound(k, 0, 1)
          
          if(!(k >= 0 && k <= 1)){
            // if(k < 0) k = 0
            // else throw [k1, k2]
            
            log(k)
            this.pause()
            return t
            
            // eval([][0]+"")
            // debugger
            throw [k1, k2]
            // if(k < 0) k = 0
            // else throw k
          }
          
          let t0_new = k * t
          
          if(t0_new >= t0) continue
          
          t0 = t0_new
          collision = [p1, p2]
          
          p1.collisions.add(p2)
        }
      }
    }
    
    // log(collision)
    
    if(collision !== null)
      t = t0

    let iters_num = 1e3
    
    outer_loop: for(let iter = 0;; iter++){
      if(iter === iters_num) throw "loop"
      
      for(let p of particles){
        p.pos1 = p.pos.add(p.vel.mul(t))
        p.collisions.clear()
      }
      
      break
      
      for(let p1 of particles){
        let {id: id1, pos1: pos1, rad: rad1} = p1
        
        for(let tile of this.iter_particle_tiles(p1)){
          for(let p2 of tile){
            if(id1 >= p2.id) continue
            if(p1.collisions.has(p2)) continue
            
            let {pos1: pos2, rad: rad2} = p2
            let rad = rad1 + rad2
            let dist = pos1.dist(pos2)
            
            if(dist < rad){
              let f = pos1.angle_to(pos2)
              let v1n = p1.vel.project(f)
              let v2n = p2.vel.project(f)
              if(v1n.len() + v2n.len() > v1n.add(v2n).len()){
                continue
              }
              
              // debugger
              // log("overlap", rad1 + rad2, dist)
              // log(t)
              if(t < 1e-5){
                for(let p of particles){
                  p.pos = p.pos1
                  p.collisions.clear()
                }
                log("paused")
                this.pause()
                return t
              }
              else t *= 0.5
              continue outer_loop
              // p1.col = new Color(1, 0, 0)
              // p2.col = new Color(1, 0, 0)
              // this.pause()
              // return t
            }
          }
        }
      }
      
      break
    }
    
    // log(t)
    
    for(let p of particles){
      p.pos = p.pos1
      p.collisions.clear()
    }
    
    this.update_grid()
    
    if(collision !== null){
      process_collision: {
        let [p1, p2] = collision
        let {pos: pos1, vel: vel1, mass: m1} = p1
        let {pos: pos2, vel: vel2, mass: m2} = p2
        
        let m = m1 + m2
        
        let v1 = vel1.len()
        let v2 = vel2.len()
        let t1 = vel1.angle()
        let t2 = vel2.angle()
        let f = pos1.angle_to(pos2)
        
        let a, b
        let s = sin(f)
        let c = cos(f)
        let c1 = cos(t1 - f)
        let c2 = cos(t2 - f)
        
        a = (v1 * c1 * (m1 - m2) + 2 * m2 * v2 * c2) / m
        b = v1 * sin(t1 - f)
        let v1x = a * c - b * s
        let v1y = a * s + b * c
        
        a = (v2 * c2 * (m2 - m1) + 2 * m1 * v1 * c1) / m
        b = v2 * sin(t2 - f)
        let v2x = a * c - b * s
        let v2y = a * s + b * c
        
        vel1 = new Vector(v1x, v1y)
        vel2 = new Vector(v2x, v2y)
        
        let k1 = m2 / m
        let k2 = m1 / m
        let ck1 = k1 * (1 - collision_friction) + (1 - k1)
        let ck2 = k2 * (1 - collision_friction) + (1 - k2)
        let v1n = vel1.project(f)
        let v2n = vel2.project(f)
        
        vel1 = vel1.add(v1n.mul(ck1 - 1))
        vel2 = vel2.add(v2n.mul(ck2 - 1))
        
        // log(p1.vel+"")
        // log(vel1+"")
        // log("")
        // log(p2.vel+"")
        // log(vel2+"")
        // throw 0
        
        p1.vel = vel1
        p2.vel = vel2
      }
      
      // log("collision")
      // this.pause()
      // return t
    }
    
    let g = gravity.mul(t)
    
    for(let p of particles){
      if(p.static) continue
      p.vel = p.vel.add(g)
    }
    
    return t
  }
  
  tick(time = 1){
    if(this.paused) return
    
    let {particles, dx_max} = this
    
    // log(O.fst(particles).pos.toString())
    
    while(time > 0){
      let vel_max = 0
      
      for(let p of particles){
        let vel = p.vel.len()
        if(vel > vel_max) vel_max = vel
      }
      
      let t = min(time, dx_max / vel_max)
      let dt = this.tick_aux(t)
    
      // log(dt)
      time -= dt
    }
    
    // log(O.fst(particles).pos.toString())
  }
}

class Particle {
  static id = 0n
  
  id = Particle.id++
  vel = Vector.ZERO
  col = Color.WHITE
  static = 0
  collisions = new Set()
  
  constructor(rad, mass, pos){
    this.rad = rad
    this.mass = mass
    this.pos = pos
    this.pos1 = pos
    
    this.diam = rad * 2
  }
  
  get_bounding_cuboid(offset = 0){
    let {pos, rad} = this
    let v = Vector.from(rad + offset)
    return new Cuboid(pos.sub(v), pos.add(v))
  }
}

class Cuboid {
  constructor(v1, v2){
    this.v1 = v1
    this.v2 = v2
  }
  
  middle(){
    let {v1, v2} = this
    return v1.middle(v2)
  }
  
  has(arg){
    if(arg instanceof Cuboid)
      return cuboid.v1.ge_all(this.v1) && cuboid.v2.le_all(this.v2)
    
    arg = Vector.from(arg)
    return arg.ge_all(this.v1) && arg.le_all(this.v2)
  }
  
  *iter_sample_vecs(dif){
    let {v1, v2} = this
    
    dif = Vector.from(dif)
    
    v1 = v1.sub_mod(dif)
    v2 = v2.sub_mod(dif)
    
    for(let y = v1.y; y <= v2.y; y += dif.y){
      for(let x = v1.x; x <= v2.x; x += dif.x){
        let v = new Vector(x, y)
        yield v.div(dif)
      }
    }
  }
  
  rand_point(){
    let {v1, v2} = this
    
    return new Vector(
      O.randf(v1.x, v2.x),
      O.randf(v1.y, v2.y),
    )
  }
}

class Vector {
  static mk_with_all_coords_eq(x){
    return new Vector(x, x)
  }
  
  static from(v){
    if(v instanceof Vector)
      return v
    
    return Vector.mk_with_all_coords_eq(v)
  }
  
  static up(len = 1){
    return new Vector(0, -len)
  }
  
  static down(len = 1){
    return new Vector(0, len)
  }
  
  static left(len = 1){
    return new Vector(-len, 0)
  }
  
  static right(len = 1){
    return new Vector(len, 0)
  }
  
  static polar(r, a){
    let c = cos(a)
    let s = sin(a)
    
    return new Vector(c * r, s * r)
  }
  
  static ZERO = new Vector(0, 0)
  static UP = Vector.up()
  static DOWN = Vector.down()
  static LEFT = Vector.left()
  static RIGHT = Vector.right()
  
  constructor(x, y){
    this.x = x
    this.y = y
  }
  
  dist(v){
    return this.sub(v).len()
  }
  
  len(len_new = null){
    let {x, y} = this
    
    let len = O.hypot(x, y)
    if(len_new === null) return len
    
    if(len === 0) return Vector.right(len_new)
    return this.mul(len_new / len)
  }
  
  norm(){
    return this.len(1)
  }
  
  add(v){
    v = Vector.from(v)
    return new Vector(this.x + v.x, this.y + v.y)
  }
  
  sub(v){
    v = Vector.from(v)
    return new Vector(this.x - v.x, this.y - v.y)
  }
  
  mul(v){
    v = Vector.from(v)
    return new Vector(this.x * v.x, this.y * v.y)
  }
  
  div(v){
    v = Vector.from(v)
    return new Vector(this.x / v.x, this.y / v.y)
  }
  
  half(){
    return this.div(2)
  }
  
  interpolate(v, k){
    v = Vector.from(v)
    return this.mul(1 - k).add(v.mul(k))
  }
  
  middle(v){
    v = Vector.from(v)
    return this.interpolate(v, 0.5)
  }
  
  le_all(v){
    v = Vector.from(v)
    return this.x <= v.x && this.y <= v.y
  }
  
  ge_all(v){
    v = Vector.from(v)
    return this.x >= v.x && this.y >= v.y
  }
  
  lt_all(v){
    v = Vector.from(v)
    return this.x < v.x && this.y < v.y
  }
  
  gt_all(v){
    v = Vector.from(v)
    return this.x > v.x && this.y > v.y
  }
  
  mod(v){
    let {x, y} = this
    v = Vector.from(v)
    return new Vector(O.mod(x, v.x), O.mod(y, v.y))
  }
  
  sub_mod(v){
    let {x, y} = this
    v = Vector.from(v)
    return new Vector(x - O.mod(x, v.x), y - O.mod(y, v.y))
  }
  
  rotate(a){
    let {x, y} = this
    let c = cos(a)
    let s = sin(a)
    
    // return new Vector(
    //   c * x - s * y,
    //   s * x + c * y,
    // )
    
    let x1 = x * cos(a)
    let y1 = x * sin(a)
    let x2 = y * cos(a + pih)
    let y2 = y * sin(a + pih)
    
    return new Vector(x1 + x2, y1 + y2)
  }
  
  angle(angle_new = null){
    let {x, y} = this
    
    if(angle_new === null)
      return atan2(y, x)
    
    return Vector.polar(this.len(), angle_new)
  }
  
  angle_to(v){
    v = Vector.from(v)
    return v.sub(this).angle()
  }
  
  project(angle){
    if(angle instanceof Vector)
      angle = angle.angle()
    
    let v = new Vector(this.rotate(-angle).x, 0)
    return v.rotate(angle)
  }
  
  neg(){
    return this.mul(-1)
  }
  
  to_list(){
    return [this.x, this.y]
  }
  
  toString(){
    return `(${this.to_list().join(", ")})`
  }
}

class VectorMap {
  map = new O.MultidimensionalMap()
  
  has(v){
    return this.map.has(v.to_list())
  }
  
  get(v){
    return this.map.get(v.to_list())
  }
  
  insert(v, val){
    this.map.set(v.to_list(), val)
  }
}

class Color {
  constructor(r, g, b){
    this.r = r
    this.g = g
    this.b = b
  }
  
  toString(){
    let {r, g, b} = this
    let s = [r, g, b].map(a => Math.round(a * 255)
      .toString(16).padStart(2, "0")).join("")
    return `#${s}`
  }
}

class Num {
  constructor(){
    
  }
  
  [Symbol.toPrimitive](){
    throw "Num[Symbol.toPrimitive]"
  }
}

main()