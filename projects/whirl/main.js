"use strict"

const assert = require("assert")

const {
  min, max, abs, floor,
  ceil, round, sqrt, sin, cos,
  atan, atan2, clz32,
  log2, sign, acos,
} = Math

const {pi, pi2, pih} = O

const ent_speed = 1
const ent_angle_epsilon = pi * 0.01
const ent_angle_offset = pi * 0.5
const ent_line_width = 3
const ent_sight_range = 10

const ents = new Set()

let canvas, g
let iw, ih
let iwh, ihh

let clicked = 0
let cx, cy

let paused = 0
let fade_enabled = 0

const main = () => {
  g = O.ceCanvas().g
  canvas = g.canvas
  
  aels()
  on_resize()
  
  {
    let ent
    
    ent = new Entity(iwh - 150, ihh, O.randf(pi2))
    ent.col = "#f00"
    ents.add(ent)
    
    ent = new Entity(iwh - 50, ihh, O.randf(pi2))
    ent.col = "#0f0"
    ents.add(ent)
    
    ent = new Entity(iwh + 50, ihh, O.randf(pi2))
    ent.col = "#f80"
    ents.add(ent)
    
    ent = new Entity(iwh + 150, ihh, O.randf(pi2))
    ent.col = "#0ff"
    ents.add(ent)
  }
  
  tick_wrap()
}

const aels = () => {
  O.ael("keydown", on_key_down)
  O.ael("mousedown", on_mouse_down)
  O.ael("mousemove", on_mouse_move)
  O.ael("mouseup", on_mouse_up)
  // O.ael("resize", on_resize)
}

const update_cur = evt => {
  cx = evt.clientX
  cy = evt.clientY
}

const on_key_down = evt => {
  let {code} = evt
  let flags = O.evtFlags(evt)
  
  if(flags === 0){
    if(code === "Space"){
      paused ^= 1
      return
    }
    
    if(code === "KeyF"){
      fade_enabled ^= 1
      return
    }
    
    return
  }
}

const on_mouse_down = evt => {
  let {button} = evt
  
  if(button === 0){
    clicked = 1
    update_cur(evt)
    return
  }
}

const on_mouse_move = evt => {
  if(clicked){
    let cx0 = cx
    let cy0 = cy
    
    update_cur(evt)
    
    g.lineWidth = 2
    g.strokeStyle = "#000"
    g.beginPath()
    g.moveTo(cx0, cy0)
    g.lineTo(cx, cy)
    g.stroke()
    
    return
  }
}

const on_mouse_up = evt => {
  let {button} = evt
  
  if(button === 0){
    clicked = 0
    return
  }
}

const on_resize = () => {
  iw = O.iw
  ih = O.ih
  iwh = iw / 2
  ihh = ih / 2
  
  g.fillStyle = "#fff"
  g.fillRect(0, 0, iw, ih)
}

const tick = () => {
  for(let ent of ents){
    ent.tick(3)
  }
  
  if(fade_enabled){
    let imgd = g.getImageData(0, 0, iw, ih)
    let {data} = imgd
    let len = data.length
    
    for(let i = 0; i !== len; i += 4){
      data[i]++
      data[i + 1]++
      data[i + 2]++
    }
    
    g.putImageData(imgd, 0, 0)
  }
}

const tick_wrap = () => {
  if(!paused) tick()
  setTimeout(tick_wrap, 16)
}

class Entity {
  col = "#000"
  
  constructor(x, y, dir){
    this.x = x
    this.y = y
    this.dir = dir
  }
  
  tick1(){
    let {x, y} = this
    
    let dir = this.calc_new_dir()
    if(dir === null) return
    
    let dx = ent_speed * cos(dir)
    let dy = ent_speed * sin(dir)
    let x1 = x + dx
    let y1 = y + dy
    
    this.x = x1
    this.y = y1
    this.dir = dir
  }
  
  tick(n = 1){
    let {x, y} = this
    
    for(let i = 0; i !== n; i++)
      this.tick1()
    
    g.lineWidth = ent_line_width
    g.strokeStyle = this.col
    g.beginPath()
    g.moveTo(x, y)
    g.lineTo(this.x, this.y)
    g.stroke()
  }
  
  calc_new_dir1(){
    let {x, y, dir} = this
    
    let d = ent_sight_range
    let d2 = d * 2 + 1
    let sx = round(x - d)
    let sy = round(y - d)
    let sw = sx + d2
    let sh = sy + d2
    let imgd = g.getImageData(sx, sy, sw, sh)
    let {data} = imgd
    
    let k0 = ent_line_width + 1
    
    const is_free = angle => {
      for(let k = k0; k <= d; k++){
        let i = round(d + cos(angle) * k)
        let j = round(d + sin(angle) * k)
        
        if(i < 0 || i >= d2 || j < 0 || j >= d2)
          return 0
        
        if((sx + i) < 0 || (sx + i) >= iw || (sy + j) < 0 || (sy + j) >= ih)
          return 0
        
        let index = j * sw + i << 2
        let red = data[index]
        let green = data[index + 1]
        let blue = data[index + 2]
        
        // if(green < 128 && !(red < 128 && blue < 128)){
        //   data[index] = 255
        //   data[index + 1] = 255
        //   data[index + 2] = 255
        // }
        
        // if(!(blue === 0xFF))
        if(!(red === 0xFF && green === 0xFF && blue === 0xFF))
          return 0
      }
      
      return 1
    }
    
    const find_first_obstacle = () => {
      for(let angle = 0; angle < pi2; angle += ent_angle_epsilon)
        if(!is_free(angle)) return angle
      
      return null
    }
    
    let angle0 = find_first_obstacle()
    
    if(angle0 === null){
      // g.putImageData(imgd, sx, sy)
      return dir
    }
    
    let angle_intvals = []
    let angle_intval = null
    let angle = null
    
    const push = () => {
      let a1 = angle_intval[0]
      let a2 = max(a1, angle - ent_angle_epsilon)
      
      angle_intval.push(a2)
      
      // if(a2 - a1 > 0)
        angle_intvals.push(angle_intval)
      
      angle_intval = null
    }
    
    for(let d_dir = 0; d_dir < pi2; d_dir += ent_angle_epsilon){
      angle = angle0 + d_dir
      
      if(is_free(angle)){
        if(angle_intval === null)
          angle_intval = [angle]
        
        continue
      }
      
      if(angle_intval !== null)
        push()
    }
    
    if(angle_intval !== null)
      push()

    if(angle_intvals.length === 0)
      return null

    let angle_intvals1 = angle_intvals
      .filter(([a1, a2]) => a2 - a1 > ent_angle_offset)
    
    if(angle_intvals1.length !== 0)
      angle_intvals = angle_intvals1
    
    let dir1 = null
    let dif_min = 0
    let intval_min = null
    
    for(let intval of angle_intvals){
      let [a1, a2] = intval
      let len = a2 - a1
      
      let a3 = len > ent_angle_offset * 2 ?
        a2 - ent_angle_offset : a1 + len / 2
      
      let dif = angle_dif(dir, a3)
        
      if(dir1 === null || dif < dif_min){
        dir1 = a3
        dif_min = dif
        intval_min = intval
      }
    }
    
    if(0) if(angle_intvals.length >= 2 || window.z){
      g.putImageData(imgd, sx, sy)
      
      for(let [a1, a2] of angle_intvals){
        let x11 = x + cos(a1) * k0
        let y11 = y + sin(a1) * k0
        let x12 = x + cos(a1) * ent_sight_range
        let y12 = y + sin(a1) * ent_sight_range
        
        let x21 = x + cos(a2) * k0
        let y21 = y + sin(a2) * k0
        let x22 = x + cos(a2) * ent_sight_range
        let y22 = y + sin(a2) * ent_sight_range
        
        g.lineWidth = 1
        g.strokeStyle = "#f0f"
        g.beginPath()
        
        g.moveTo(x11, y11)
        g.lineTo(x12, y12)
        g.moveTo(x21, y21)
        g.lineTo(x22, y22)
        g.stroke()
        
        g.beginPath()
        g.arc(x, y, k0, a1, a2)
        g.stroke()
        
        g.beginPath()
        g.arc(x, y, ent_sight_range, a1, a2)
        g.stroke()
        
        let x31 = x + cos(dir1) * k0
        let y31 = y + sin(dir1) * k0
        let x32 = x + cos(dir1) * ent_sight_range
        let y32 = y + sin(dir1) * ent_sight_range
        
        g.lineWidth = 1
        g.strokeStyle = "#00f"
        
        g.beginPath()
        g.moveTo(x31, y31)
        g.lineTo(x32, y32)
        g.stroke()
      }
      
      paused = 1
    }
    
    return dir1
  }
  
  calc_new_dir(){
    let dir = this.calc_new_dir1()
    if(dir === null) return null
    
    return dir % pi2
  }
}

const angle_dif = (dir1, dir2) => {
  let ax = cos(dir1)
  let ay = sin(dir1)
  let bx = cos(dir2)
  let by = sin(dir2)
  let cs = ax * bx + ay * by
  let sgn = sign(ax * by - ay * bx)
  let dif = sgn !== 0 ? acos(cs) * sgn :
    ax === bx && ay === by ? 0 : pi
  
  return abs(dif)
}

(async () => main())().catch(log)