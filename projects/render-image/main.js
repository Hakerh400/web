'use strict';

const assert = require('assert');

const {sin, cos, atan2} = Math;

const {pi, pih, pi2} = O;

const size = 70
const lineWidth = 3
const fontBig = 32
const fontSmall = 20

const cols = {
  bg: 'white',
  text: 'black',
  lightLines: 'rgb(200, 191, 231)',
  darkLines: 'black',
};

const {g} = O.ceCanvas();
const {canvas} = g;

const d = size * 2 * cos(pi / 5);

let iw, ih, iwh, ihh;

const main = () => {
  aels();
  onResize();
};

const aels = () => {
  O.ael('resize', onResize);
};

const onResize = () => {
  iw = O.iw;
  ih = O.ih;
  iwh = iw / 2;
  ihh = ih / 2;
  
  canvas.width = iw;
  canvas.height = ih;
  
  g.lineWidth = lineWidth;
  g.textBaseline = 'middle';
  g.textAlign = 'center';
  g.lineCap = 'round';
  g.lineJoin = 'round';
  
  render();
};

const render = () => {
  g.fillStyle = cols.bg;
  g.fillRect(0, 0, iw, ih);
  
  const N = null;
  
  drawPentaInfoList([
    [
      0, 0, N, 0,
      5, 4, 2, 0,
      10,
    ], [
      N, 1, 0, 0,
      1, 4, 2, 0,
      6, 0, 3, 0,
      11,
    ],
    [
      N, 2, 1, 0,
      2, 4, 2, 0,
      7,
    ],
    [
      N, 3, N, 0,
      3, 4, 2, 0,
      8,
    ],
    [
      N, 4, N, 0,
      4, 4, 2, 0,
      9,
    ],
  ]);
};

const drawPentaCoords = (x, y, angle) => {
  g.beginPath();
  O.drawPolygon(g, x, y, size, 5, angle + pi2 / 5);
  g.stroke();
};

const drawPentaPath = pth => {
  const d = size * 2 * cos(pi / 5);
  
  let x = iwh;
  let y = ihh;
  let angle = -pih;
  
  for(const dir of pth){
    angle += (dir + .5) * pi2 / 5;
    x += d * cos(angle);
    y += d * sin(angle);
  }
  
  drawPentaCoords(x, y, angle);
};

const drawPentaPaths = pths => {
  for(const pth of pths)
    drawPentaPath(pth);
};

const drawPentaPathsUnion = pths => {
  const st = new Set(['']);
  
  for(const pth of pths){
    let pthNew = '';
    
    for(const dir of pth){
      pthNew += dir;
      st.add(pthNew);
    }
  }
  
  drawPentaPaths([...st].map(a => [...a].map(a => a | 0)));
};

const drawPentaInfoList = infoList => {
  g.strokeStyle = cols.lightLines;
  setFont(fontBig);
  
  const infoListLen = infoList.length;
  const triangles = [];
  
  for(let i = 0; i !== infoListLen; i++){
    const info = infoList[i];
    const len = info.length;
    const len1 = len - 1;
    
    let x = iwh;
    let y = ihh;
    let angle = -pih;
    
    if(i === 0) drawPentaCoords(x, y, angle);
    
    const drawPentaText = s => {
      if(s === null) return;
      
      g.fillStyle = cols.text;
      g.fillText(s, x, y);
    };
    
    for(let i = 0; i !== len1; i += 4){
      const n = info[i];
      const dir = info[i + 1];
      const n1 = info[i + 2];
      const n2 = info[i + 3];
      
      drawPentaText(n);
      
      angle += (dir + .5) * pi2 / 5;
      
      const x1 = x + d * cos(angle);
      const y1 = y + d * sin(angle);
      
      triangles.push(
        [n1, x, y, angle],
        [n2, x1, y1, angle + pi],
      );
      
      x = x1;
      y = y1;
      
      drawPentaCoords(x, y, angle);
    }
    
    const n = O.last(info);
    drawPentaText(n);
  }
  
  g.strokeStyle = cols.darkLines;
  setFont(fontSmall);
  
  for(const t of triangles)
    drawPentaTriangle(...t);
  
  // const pths = info.map(a => a.filter((a, i) => i % 4 === 1));
  // drawPentaPathsUnion(pths);
};

const drawPentaTriangle = (n, x, y, angle) => {
  if(n === null) return;
  
  const factor = .7;
  const d1 = d * (1 - factor) / 2;
  const d2 = d * (1 - factor * .4) / 2;
  const a1 = angle - pi / 5;
  const a2 = angle + pi / 5;
  
  g.beginPath();
  g.moveTo(x + size * cos(a1), y + size * sin(a1));
  g.lineTo(x + size * cos(a2), y + size * sin(a2));
  g.lineTo(x + d1 * cos(angle), y + d1 * sin(angle));
  g.closePath();
  g.stroke();
  
  g.fillText(n, x + d2 * cos(angle), y + d2 * sin(angle));
};

const setFont = n => {
  g.font = `${n}px arial`;
};

main();