'use strict';

const assert = require('assert');

const cols = {
  canvasBg: 'darkgray',
  active: '#ff4',
  done: '#4f4',
};

module.exports = cols;