'use strict';

const assert = require('assert');
const cols = require('./cols');

class Tile{
  constructor(grid, x, y){
    this.grid = grid;
    this.x = x;
    this.y = y;
    
    this.stage = x === 0 && y === 0 ? 1 : 0;
    this.lines = O.ca(4, () => this.stage === 1 ? 1 : 0);
  }
  
  adjEmpty(dir){
    return getEmpty(this.grid.nav(this, dir));
  }
  
  adjsEmpty(){
    return O.ca(4, i => this.adjEmpty(i));
  }
  
  adjsDiagEmpty(){
    const {grid, x, y} = this;
    
    return [
      [1, -1],
      [1, 1],
      [-1, 1],
      [-1, -1],
    ].map(([dx, dy]) => {
      return getEmpty(grid.get(x + dx, y + dy));
    });
  }
  
  render(g){
    const {stage} = this;
    
    if(stage !== 0){
      g.fillStyle = stage === 1 ? cols.active : cols.done;
      g.fillRect(0, 0, 1, 1);
    }
  }
  
  renderLines(g){
    const {lines} = this;
    const {gs} = g;
    const n = 1 + gs;
    
    g.beginPath();
    if(lines[0]){
      g.moveTo(0, 0);
      g.lineTo(n, 0);
    }
    if(lines[1]){
      g.moveTo(1, 0);
      g.lineTo(1, n);
    }
    if(lines[2]){
      g.moveTo(0, 1);
      g.lineTo(n, 1);
    }
    if(lines[3]){
      g.moveTo(0, 0);
      g.lineTo(0, n);
    }
    g.stroke();
  }
}

const getEmpty = d => {
  if(d === null) return null;
  if(d.stage !== 0) return null;
  
  return d;
};

module.exports = Tile;