'use strict';

const assert = require('assert');
const cols = require('./cols');
const Tile = require('./tile');

const w = 48;
const h = 27;
const size = 40;

const {g} = O.ceCanvas(1);
const {canvas} = g;

let iw, ih;
let iwh, ihh;

let grid = null;

const main = () => {
  grid = initGrid();
  
  initGrid();
  aels();
  onResize();
};

const initGrid = () => {
  const grid = new O.Grid(w, h, (x, y, grid) => {
    return new Tile(grid, x, y);
  });
  
  return grid;
};

const aels = () => {
  O.ael('keydown', onKeyDown);
  O.ael('resize', onResize);
};

const onKeyDown = evt => {
  const {code} = evt;
  const flags = O.evtFlags(evt);
  
  if(flags === 0){
    if(code === 'Enter'){
      step();
      return;
    }
    
    return;
  }
};

const onResize = evt => {
  iw = O.iw;
  ih = O.ih;
  iwh = iw / 2;
  ihh = ih / 2;
  
  canvas.width = iw;
  canvas.height = ih;
  
  render();
};

const step = () => {
  const actives = new Set();
  
  grid.iter((x, y, d) => {
    const {stage, lines} = d;
    if(stage !== 1) return;
    
    const getDirLists = () => {
      const adjs = d.adjsEmpty().map(a => a !== null);
      const diags = d.adjsDiagEmpty().map(a => a !== null);
      
      const lists = [];
      let list = [];
      
      for(let i = 0; i !== 4; i++){
        if(!adjs[i]){
          if(list.length === 0) continue;
          lists.push(list);
          list = [];
          continue;
        }
        
        list.push(i);
        
        if(!diags[i]){
          lists.push(list);
          list = [];
        }
      }
      
      if(list.length !== 0)
        lists.push(list);
      
      merge: {
        if(!diags[3]) break merge;
        
        const i = lists.findIndex(list => list.includes(0));
        if(i === -1) break merge;
        
        const j = lists.findIndex(list => list.includes(3));
        if(j === -1) break merge;
        
        const xs = lists[i];
        const ys = lists[j];
        
        lists.splice(j, 1);
        
        for(const y of ys)
          xs.push(y)
      }
      
      return lists;
    };
    
    const lists = getDirLists();

    for(const list of lists){
      const dir = O.randElem(list);
      d.lines[dir] = 0;
      
      const d1 = grid.nav(d, dir);
      actives.add(d1);
      
      const dir1 = dir + 2 & 3;
      const lines1 = d1.lines;
      
      for(let i = 0; i !== 4; i++){
        if(i === dir1) continue;
        lines1[i] = 1;
      }
    }
    
    d.stage = 2;
  });
  
  for(const d of actives)
    d.stage = 1;
  
  render();
};

const render = () => {
  g.fillStyle = cols.canvasBg;
  g.fillRect(0, 0, iw, ih);
  
  g.translate(iwh, ihh);
  g.scale(size);
  g.translate(-w / 2, -h / 2);
  
  const {gs} = g;
  
  g.strokeStyle = '#888';
  g.beginPath();
  for(let y = 0; y <= h; y++){
    g.moveTo(0, y);
    g.lineTo(w + gs, y);
  }
  for(let x = 0; x <= w; x++){
    g.moveTo(x, 0);
    g.lineTo(x, h + gs);
  }
  g.stroke();
  g.strokeStyle = 'black';
  
  grid.iter((x, y, d) => {
    g.save();
    g.translate(x, y);
    d.render(g);
    g.restore();
  });
  
  grid.iter((x, y, d) => {
    g.save();
    g.translate(x, y);
    d.renderLines(g);
    g.restore();
  });
  
  g.resetTransform();
};

main();