"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")
const Code = require("./code")

let events_enabled = 0

let str = ""
let main_div = null
let items = []

const main = async () => {
  O.ael("keydown", async evt => {
    let {code} = evt
    let flags = O.evtFlags(evt)
    
    if(flags === 4){
      if(code === "KeyR"){
        location.reload()
        return
      }
        
      return
    }
  })
  
  await O.addStyle("style.css")
  
  main_div = O.ceDiv(O.body, "main")
  
  ael("keydown", async evt => {
    let {code, key} = evt
    let flags = O.evtFlags(evt)
    
    if(flags === 0 || flags === 2){
      if(key.length === 1){
        str += key
        return
      }
    }
    
    if(flags === 0){
      if(code === "Enter"){
        let s = str
        str = ""
        await on_input(s)
        
        return
      }
      
      return
    }

    if(flags === 4){
      if(/^[\+\-]$/.test(key)){
        evt.preventDefault()
        return
      }
        
      return
    }
  })
  
  enable_events()
}

const on_input = async str => {
  let item = new Item()
  await item.init(str)
}

class Item {
  async init(str){
    let items_num = items.length
    let has_items = items_num !== 0
    let fst_item = has_items ? items[0] : null
    
    if(has_items && fst_item.str === str){
      fst_item.inc_cnt()
      return fst_item
    }
    
    let div = O.ceDiv(main_div, "item")

    if(has_items)
      main_div.insertBefore(div, fst_item.div)

    items.unshift(this)

    this.div = div
    this.str = str
    this.y = 0
    this.cnt = 1
    
    this.set_y()
    
    let info_div = O.ceDiv(div, "item-info")
    let cnt_div = O.ceDiv(div, "item-cnt")
    
    this.info_div = info_div
    this.cnt_div = cnt_div
    // this.update_cnt()
    
    let code_div = O.ceDiv(info_div, "item-code")
    let code = await Code.str_to_code(str)
    code_div.appendChild(code)
    
    let str_div = O.ceDiv(info_div, "item-str")
    str_div.innerText = str
    
    let rect = div.getBoundingClientRect()
    let h = rect.height
    
    div.classList.add("invisible")
    
    this.h = h
    
    await O.rafa2(() => {
      let {ih} = O
      let n = null
      
      for(let i = 1; i !== items.length; i++){
        let item = items[i]
        let div1 = item.div
        
        if(n !== null || div1.getBoundingClientRect().top > ih){
          div1.remove()
          if(n === null) n = i
        }
        
        item.add_y(h + 16)
      }
      
      if(n !== null)
        items.length = n
      
      div.classList.remove("invisible")
      div.classList.add("visible")
    })
  }
  
  set_y(y = null){
    let {div} = this
    
    if(y === null) y = this.y
    else this.y = y
    
    let rect = O.body.getBoundingClientRect()
    let y0 = rect.top
    
    div.style.top = `${y0 + y}px`
  }
  
  add_y(dy){
    this.set_y(this.y + dy)
  }
  
  update_cnt(){
    this.cnt_div.innerText = `x${this.cnt}`
  }
  
  set_cnt(cnt){
    this.cnt = cnt
    this.update_cnt()
  }
  
  add_cnt(n){
    this.set_cnt(this.cnt + n)
  }
  
  inc_cnt(){
    this.add_cnt(1)
  }
}

const enable_events = () => {
  events_enabled = 1
}

const ael = (type, fn) => {
  let fn1 = evt => {
    if(!events_enabled) return
    events_enabled = 0
    
    const f = async () => {
      await fn(evt)
    }
    
    f().catch(on_err).finally(() => {
      events_enabled = 1
    })
  }
  
  O.ael(type, fn1)
}

const on_err = err => {
  // log(err)
  throw err
}

main().catch(on_err)