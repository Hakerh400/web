"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")

const w = 560
const h = 100
const bars_num = 60

const bar_size = w / bars_num

const str_to_code = async str => {
  O.enhancedRNG = 1
  O.rseed = 0
  O.randState = O.sha256(str)
  
  let canvas = O.doc.createElement("canvas")
  let g = canvas.getContext("2d")
  
  canvas.width = w
  canvas.height = h
  
  g.fillStyle = "black"
  
  for(let i = 0; i !== bars_num; i++){
    let x = bar_size * i
    let ws = O.randf(bar_size)
    
    g.fillRect(x, 0, ws, h)
  }
  
  return canvas
}

module.exports = {
  str_to_code,
}