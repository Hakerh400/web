'use strict';

const assert = require('assert');

const iw = 1920;
const ih = 1080;
const speed = 1e4;

const cols = [
  [0, 255, 255],
  [255, 0, 255],
].map(a => new Uint8ClampedArray(a));

const ids_num = cols.length;

const {g} = O.ceCanvas();
const {canvas} = g;

let grid = null;
let imgd = null;
let data = null;

const main = () => {
  resize();
  
  imgd = g.getImageData(0, 0, iw, ih);
  data = imgd.data;
  
  grid = new O.Grid(iw, ih, (x, y) => {
    const n = 0;
    const id = 0;
    
    set_pix(x, y, id);
    
    return [n, id];
  });
  
  put_imgd();
  O.raf(update);
};

const resize = () => {
  canvas.width = iw;
  canvas.height = ih;
  
  g.fillStyle = 'white';
  g.fillRect(0, 0, iw, ih);
};

const update = () => {
  for(let sp = 0; sp !== speed; sp++){
    const x = O.rand(iw);
    const y = O.rand(ih);
    const d = grid.get(x, y);
    const [n, id] = d;
    const n1 = (n + 1) % 3;
    
    if(n !== 0) continue;
    
    const found = grid.adj(x, y, (x1, y1, d1) => {
      if(d1 === null) return 1;
      if(d1[0] === n1) return 1;
      return 0;
    });
    
    if(!found) continue;
    
    const id1 = (d[1] + 1) % ids_num;
    
    d[0] = n1;
    d[1] = id1;
    
    set_pix(x, y, id1);
  }
  
  put_imgd();
  O.raf(update);
};

const set_pix = (x, y, id) => {
  const col = cols[id];
  const i = x + y * iw << 2;
  
  data[i] = col[0];
  data[i + 1] = col[1];
  data[i + 2] = col[2];
};

const put_imgd = () => {
  g.putImageData(imgd, 0, 0);
};

main();