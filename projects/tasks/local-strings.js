'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');

const cwd = __dirname;
const lsFile = path.join(cwd, 'local-strings.txt');

const localStrings = O.sanl(await O.rfs(lsFile));

const func = (n, ...args) => {
  return localStrings[n - 1].replace(/\_/g, () => args.shift());
};

module.exports = func;