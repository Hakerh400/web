'use strict';

const assert = require('assert');

const speed = 3e4;
const interval = 1e3;
const brush_size = 20;

const rad_range = [5, 20];
const cp_range = [30, 50];

const iw = 1920;
const ih = 1080;

const {g} = O.ceCanvas();
const {canvas} = g;

const proj = O.project;

// let iw = null;
// let ih = null;
let px = null;
let py = null;

let paused = 1
let modify_flag = 1

const main = () => {
  aels();
  on_resize();
  update_canvas();
};

const aels = () => {
  // O.ael('resize', on_resize);
  O.ael('keydown', on_key_down);
  O.ael('mousedown', on_mouse_down);
  O.ael('mouseup', on_mouse_up);
  O.ael('mousemove', on_mouse_move);
  O.ael('beforeunload', on_before_unload);
};

const update_canvas = () => {
  if(!paused){
    const imgd = g.getImageData(0, 0, iw, ih);
    const {data} = imgd;
    
    for(let sp = 0; sp !== speed; sp++){
      const y = O.rand(ih);
      const x = O.rand(iw);
      const i = y * iw + x << 2;
      
      data[i]++;
      data[i + 1]++;
      data[i + 2]++;
    }
    
    g.putImageData(imgd, 0, 0);
    
    modify: if(modify_flag && O.randf(interval) < 1){
      const t = O.rand(2);
      
      if(t === 0){
        const rad = O.randf(...rad_range);
        const x = O.rand(iw);
        const y = O.rand(ih);
        
        g.fillStyle = 'black';
        g.beginPath();
        g.arc(x, y, rad, 0, O.pi2);
        g.fill();
        
        break modify;
      }
      
      if(t === 1){
        const w = O.rand(...cp_range);
        const h = O.rand(...cp_range);
        const x0 = O.rand(iw - w);
        const y0 = O.rand(ih - h);
        const x = O.rand(iw - w);
        const y = O.rand(ih - h);
        
        g.drawImage(canvas, x0, y0, w, h, x, y, w, h);
        
        break modify;
      }
    }
  }
  
  O.raf(update_canvas);
};

const on_resize = () => {
  // let canvas1 = null;
  
  // if(iw !== null){
  //   canvas1 = O.doc.createElement('canvas');
  //   canvas1.width = iw;
  //   canvas1.height = ih;
  // 
  //   const g1 = canvas1.getContext('2d');
  //   g1.drawImage(canvas, 0, 0);
  // }
  
  // iw = O.iw;
  // ih = O.ih;
  
  canvas.width = iw;
  canvas.height = ih;
  
  g.lineCap = 'round';
  g.lineJoin = 'round';
  g.lineWidth = brush_size;
  
  g.fillStyle = 'white';
  g.fillRect(0, 0, iw, ih);
  
  // if(canvas1 === null){
  if(O.has(localStorage, proj)){
    const img = new Image();
    img.onload = () => g.drawImage(img, 0, 0);
    img.src = localStorage[proj];
  }
  // }else{
  //   g.drawImage(canvas1, 0, 0);
  // }
};

const on_key_down = evt => {
  let {code} = evt
  
  if(code === "Space"){
    paused ^= 1
    return
  }
  
  if(code === "KeyM"){
    modify_flag ^= 1
    return
  }
  
  if(code === "KeyR"){
    g.fillStyle = "white"
    g.fillRect(0, 0, iw, ih)
    return
  }
};

const on_mouse_down = evt => {
  const {button} = evt;
  
  if(button === 0){
    const x = evt.clientX;
    const y = evt.clientY;
    
    px = x;
    py = y;
    
    g.fillStyle = 'black';
    g.beginPath();
    g.arc(px, py, brush_size / 2, 0, O.pi2);
    g.fill();
    
    return;
  }
};

const on_mouse_up = evt => {
  const {button} = evt;
  
  if(button === 0){
    px = null;
    py = null;
    
    return;
  }
};

const on_mouse_move = evt => {
  if(px === null) return;
  
  const x = evt.clientX;
  const y = evt.clientY;
  
  g.strokeStyle = 'black';
  g.beginPath();
  g.moveTo(px, py);
  g.lineTo(x, y);
  g.stroke();
  
  px = x;
  py = y;
};

const on_before_unload = evt => {
  localStorage[proj] = canvas.toDataURL();
};

main();