"use strict"

const assert = require("assert")

const {
  min, max, abs, floor,
  ceil, round, sqrt, sin, cos,
  atan, atan2, clz32,
  log2,
} = Math

const {pi, pi2, pih} = O

const offset = 5

let paused = 1

let canvas = null
let g = null

let iw = null
let ih = null
let iwh = null
let ihh = null

const main = () => {
  init_canvas()
  on_resize()
  aels()
  
  render_wrap()
}

const aels = () => {
  O.ael("keydown", on_key_down)
}

const on_key_down = evt => {
  let {code} = evt
  let flags = O.evtFlags(evt)
  
  if(flags === 0){
    if(code === "Space"){
      paused ^= 1
      return
    }
    
    return
  }
}

const render = () => {
  draw_trunk()
}

const render_wrap = () => {
  if(!paused) render()
  O.raf(render_wrap)
}

const draw_trunk = () => {
  // let factor = s => 1
  let factor = s => s > 0 ? 0.9 : 1.05
  let d0 = O.randf(30, 100)
  let d = d0
  
  // let p1 = O.rand() ?
  //   [O.randf(iw), O.rand() ? -offset : ih + offset] :
  //   [O.rand() ? -offset : iw + offset, O.randf(ih)]
  // let p2 = move_r(p1, d)
  
  let p1 = [O.randf(0, iw), ih + 2]
  // let p2 = move(p1, pi / 6, d)
  let p2 = move(p1, 0, d)
  
  let col1 = new O.Color(128, 64, 0)
  let col2 = new O.Color(0, 255, 0)
  let col = new O.Color(0, 0, 0)
  let s = 1
  
  draw_line(mid(p1, p2), p2)
  
  for(let i = 0; i !== 1e3; i++){
    let d = dist(p1, p2) * (sqrt(3) / 2) * factor(s)
    if(d < 1) break
    
    let m = mid(p1, p2)
    let a = angle(p2, p1) + pih * s
    let p3 = move(p2, a, d)
    
    p3 = move_r(p3, O.randf(d * 0.3))
    
    let k = O.bound(d / d0, 0, 1) ** 0.6
    let k1 = 1 - k
    
    for(let i = 0; i !== 3; i++)
      col[i] = col1[i] * k + col2[i] * k1
    
    col.updateStr()
    draw_triangle(p2, p3, m, col)
    
    p1 = p3
    p2 = m
    s *= -1
    
    if(wrap([p1, p2])) break
  }
}

const wrap = ps => {
  let h_wrap = 1
  let v_wrap = 1
  
  for(let [x, y] of ps){
    if(x > -offset && x < iw + offset) h_wrap = 0
    if(y > -offset && y < ih + offset) v_wrap = 0
  }
  
  if(!(h_wrap || v_wrap)) return 0
  
  for(let p of ps){
    if(h_wrap) p[0] = iw - p[0]
    if(v_wrap) p[1] = ih - p[1]
  }
  
  return 1
}

const draw_line = (p1, p2) => {
  g.beginPath()
  g.moveTo(...p1)
  g.lineTo(...p2)
  g.stroke()
}

const draw_triangle = (p1, p2, p3, col = null) => {
  g.beginPath()
  g.moveTo(...p1)
  g.lineTo(...p2)
  g.lineTo(...p3)
  
  if(col !== null){
    g.fillStyle = col
    g.fill()
  }
  
  g.stroke()
}

const move = ([x, y], a, d) => {
  return [x + cos(a) * d, y + sin(a) * d]
}

const move_r = (p, d) => {
  return move(p, O.randf(0, pi2), d)
}

const mid = ([x1, y1], [x2, y2]) => {
  return [(x1 + x2) / 2, (y1 + y2) / 2]
}

const dist = ([x1, y1], [x2, y2]) => {
  return O.dist(x1, y1, x2, y2)
}

const angle = ([x1, y1], [x2, y2]) => {
  return atan2(y2 - y1, x2 - x1)
}

const on_resize = () => {
  iw = O.iw
  ih = O.ih
  iwh = iw / 2
  ihh = ih / 2
  
  canvas.width = iw
  canvas.height = ih
  
  g.resetTransform()
  g.fillStyle = "black"
  g.strokeStyle = "black"
  g.lineWIdth = 1
  g.lineCap = "round"
  g.lineJoin = "round"
}

const init_canvas = () => {
  g = O.ceCanvas().g
  canvas = g.canvas
}

main()