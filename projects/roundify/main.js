'use strict';

const assert = require('assert');

const {
  PI, min, max, abs, floor,
  ceil, round, sqrt, sin, cos,
  atan, atan2, clz32,
  log2,
} = Math;

const cols = {
  bg: 'white',
};

const {g} = O.ceCanvas();

let points = [];

const main = () => {
  const {iw, ih} = O;
  const iwh = iw / 2;
  const ihh = ih / 2;

  const s = 100;
  const sh = s / 2;
  const x = iwh - sh;
  const y = ihh - sh;

  add_loop(points, [
    x, y,
    x + s, y + s,
    x, y + s,
  ]);

  render();
};

const render_points = (ps, col=null) => {
  const ps_num = ps.length;

  if(col !== null) g.strokeStyle = col;
  g.beginPath();

  for(let i = 0; i < ps_num; i += 2){
    let x = ps[i];
    let y = ps[i + 1];
    g.lineTo(x, y);
  }

  g.closePath();
  g.stroke();
};

const render = () => {
  const {iw, ih} = O;

  g.fillStyle = cols.bg;
  g.fillRect(0, 0, iw, ih, 5);

  g.lineWidth = 2;

  const n = 20;

  render_points(points);

  for(let i = 0; i !== n; i++){
    points = expand(points, 10);
    render_points(points, O.Color.hsv(i / n));
  }
};

const add_line = (points, x1, y1, x2, y2, step=1) => {
  const dx = x2 - x1;
  const dy = y2 - y1;
  const len = O.hypot(dx, dy);
  const angle = atan2(dy, dx);
  const sx = cos(angle);
  const sy = sin(angle);

  for(let d = 0; d < len; d += step){
    const k = min(d, len);
    points.push(x1 + sx * k, y1 + sy * k);
  }

  return points;
};

const add_lines = (points, ps, step) => {
  const ps_num = ps.length;

  let xp = null;
  let yp = null;

  for(let i = 0; i < ps_num; i += 2){
    const x = ps[i];
    const y = ps[i + 1];

    if(xp !== null)
      add_line(points, xp, yp, x, y, step);
    
    xp = x;
    yp = y;
  }

  return points;
};

const close_loop = (ps, slice=0) => {
  if(slice) ps = ps.slice();

  ps.push(ps[0], ps[1]);
  return ps;
};

const add_loop = (points, ps, step) => {
  return add_lines(points, close_loop(ps, 1), step);
};

const roundify = (ps, fac) => {
  const fac2 = fac * 2;
  const ps_num = ps.length;
  const ps_new = [];

  let xs = 0;
  let ys = 0;

  for(let i = 0; i < fac2; i += 2){
    const x = ps[i];
    const y = ps[i + 1];

    xs += x;
    ys += y;
  }

  const get = i => ps[i % ps_num];

  for(let i = 0; i < ps_num; i += 2){
    xs += get(i + fac2) - get(i);
    ys += get(i + 1 + fac2) - get(i + 1);
    
    ps_new.push(xs / fac, ys / fac);
  }
  
  return ps_new;
};

const expand = (ps, fac, fill=1, rnd=1) => {
  if(fill) ps = add_loop([], ps, 1 / fac);
  if(rnd) ps = roundify(ps, fac);

  const ps_num = ps.length;
  const ps_new = [];
  
  const get = i => ps[(i % ps_num + ps_num) % ps_num];
  
  for(let i = 0; i < ps_num; i += 2){
    const j = i + 1;

    const xp = get(i);
    const yp = get(j);
    const x1 = get(i - 2);
    const y1 = get(j - 2);
    const x2 = get(i + 2);
    const y2 = get(j + 2);

    const angle = atan2(y2 - y1, x2 - x1) - O.pih;
    const x = xp + fac * cos(angle);
    const y = yp + fac * sin(angle);

    ps_new.push(x, y);
  }

  return ps_new;
};

main();