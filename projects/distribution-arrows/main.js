'use strict';

const assert = require('assert');

const speed = 1;

const {g} = O.ceCanvas();

const w = window.iw || O.iw;
const h = window.ih || O.ih;

let imgd = null;
let grid = null;

const col = new Uint8ClampedArray(3);

const main = () => {
  const {canvas} = g;
  
  canvas.width = w;
  canvas.height = h;
  
  imgd = mk_imgd();
  grid = mk_grid();
  
  animate();
};

const mk_imgd = () => {
  imgd = new O.ImageData(g, 1);
  
  return imgd;
};

const mk_grid = () => {
  const grid = new O.Grid(w, h, (x, y) => {
    // const arrows = O.dist(x, y, 300, 300) < 100
    //   ? norm([2, 3, 4, 1, 5])
    //   : rand_part(5);
    
    const dir = Math.atan2(200 - y, 200 - x) + .1;
    const c = Math.cos(dir);
    const s = Math.sin(dir);
    
    const arrows = [
      Math.max(c, 0),
      Math.max(s, 0),
      Math.max(-c, 0),
      Math.max(-s, 0),
      0,
    ];
    
    for(let i = 0; i !== 5; i++)
      arrows[i] = O.randf(arrows[i]);
    
    return {
      arrows: norm(arrows),
      val: 1,
      val1: 0,
    }
  });
  
  return grid;
};

const update_grid = grid => {
  grid.iterate((x, y, d) => {
    const {val, arrows} = d;
    
    for(let dir = 0; dir <= 3; dir++){
      const d1 = grid.nav1(x, y, dir, 1);
      d1.val1 += val * arrows[dir];
    }
    
    d.val1 += val * arrows[4];
  });
  
  grid.iterate((x, y, d) => {
    d.val = d.val1;
    d.val1 = 0;
  });
};

const update = () => {
  update_grid(grid);
};

const animate = () => {
  render();
  
  for(let i = 0; i !== speed; i++)
    update();
  
  O.raf(animate);
};

const render = () => {
  const factor = calc_factor(grid);
  
  grid.iterate((x, y, d) => {
    const k = Math.min(d.val / factor, 1);
    
    O.hsv(k ** 2, col);
    
    for(let i = 0; i !== 3; i++)
      col[i] *= k ** .5;
    
    imgd.set(x, y, col);
  });
  
  imgd.put();
};

const calc_factor = grid => {
  const max = calc_max(grid);
  return Math.log2(max);
};

const calc_max = grid => {
  let m = 0;
  
  grid.iterate((x, y, d) => {
    const v = d.val;
    if(v > m) m = v;
  });
  
  return m;
};

const rand_part = (n, a=0, b=1, f=O.randf) => {
  const xs = O.sortAsc(O.ca(n - 1, () => f(a, b))).map(x => x + a);
  
  xs.unshift(a);
  xs.push(b);
  
  return O.ca(n, i => xs[i + 1] - xs [i]);
};

const norm = xs => {
  const sum = xs.reduce((a, b) => a + b);
  return xs.map(a => a / sum);
};

main();