'use strict';

const assert = require('assert');

const year = 2023
const size = 100

const box_w = size
const box_h = size
const month_box_w = box_w * 8
const month_box_h = box_h * 8
const month_box_sp_hor = 20
const month_box_sp_ver = 20
const font_size = box_h * 0.65

const month_names = [
  'january',
  'february',
  'march',
  'april',
  'may',
  'june',
  'july',
  'august',
  'september',
  'october',
  'november',
  'december',
];

const week_letters = 'mtwtfss';

const shift_cols = [
  '#ffff00',
  '#ff0000',
  '#00ff00',
  '#0044ff',
];

const {g} = O.ceCanvas(1);

const iw = month_box_sp_hor + (month_box_w + month_box_sp_hor) * 3;
const ih = month_box_sp_ver + (month_box_h + month_box_sp_ver) * 4;

const main = () => {
  g.resize(iw, ih);

  // aels();

  // on_resize();
  render();
};

// const aels = () => {
//   O.ael('resize', on_resize);
// };

// const on_resize = evt => {
//   const {iw, ih, iwh, ihh} = O;
  
//   g.resize(iw, ih);
//   g.font(font_size);

//   render();
// };

const render = () => {
  g.fillStyle = 'black';
  g.fillRect(0, 0, iw, ih);

  g.translate(month_box_sp_hor, month_box_sp_ver);

  const render_month = month => {
    month--;

    const xt = month % 3;
    const yt = month / 3 | 0;
    const x0 = xt * (month_box_w + month_box_sp_hor);
    const y0 = yt * (month_box_h + month_box_sp_ver);

    g.fillStyle = 'white';
    g.beginPath();
    g.rect(x0, y0, month_box_w, month_box_h);
    g.fill();
    g.stroke();

    const title = `${month_names[month].toUpperCase()} ${year}`;

    g.font(font_size);
    g.fillStyle = 'black';
    g.fillText(title, x0 + box_w * 4, y0 + box_h * 0.5);

    const date = new Date(0);

    date.setFullYear(year);
    date.setMonth(month);
    date.setDate(1);

    const fst_week_day = (date.getDay() + 6) % 7;

    for(let y = 1; y !== 8; y++){
      for(let x = 0; x !== 8; x++){
        g.save();
        g.translate(x0 + box_w * x, y0 + box_h * y);
        g.scale(box_w, box_h);

        if(x >= 2){
          g.fillStyle = 'darkgray';
          g.fillRect(0, 0, 1, 1);
        }

        const draw_content = () => {
          if(x === 0){
            let str = week_letters[y - 1].toUpperCase();

            g.font(font_size);
            g.fillStyle = 'black';
            g.fillText(str, 0.5, 0.5);

            return;
          }

          if(x === 1){
            g.beginPath();
            g.rect(0, 1 / 3, 1, 1 / 3);
            g.stroke();

            g.font(font_size / 3);
            g.fillStyle = 'black';

            for(let i = 0; i !== 3; i++){
              const str = 'I'.repeat(i + 1);
              g.fillText(str, .5, 1 / 6 + i / 3);
            }

            return;
          }

          const day = (x - 2) * 7 + y - 1 - fst_week_day;
          const d = new Date(date);

          d.setDate(day + 1);

          if(d.getMonth() !== month) return;

          const shifts = O.ca(4, k => [k, get_shift(k, d)]).
            filter(a => a[1] !== 0).
            sort((a, b) => a[1] - b[1]).
            map(a => a[0]);
          
          for(let i = 0; i !== 3; i++){
            g.fillStyle = shift_cols[shifts[i]];
            g.fillRect(0, i / 3, 1, 1 / 3);
          }

          g.beginPath();
          g.rect(0, 1 / 3, 1, 1 / 3);
          g.stroke();

          g.font(font_size);
          g.fillStyle = 'black';
          render_text(g, day + 1, 0.5, 0.5);
        };

        draw_content();

        g.beginPath();
        g.rect(0, 0, 1, 1);
        g.stroke();

        g.restore();
      }
    }
  };

  for(let month = 1; month <= 12; month++)
    render_month(month);

  g.resetTransform();
};

const render_text = (g, str, x, y) => {
  g.fillText(str, x, y);

  // g.lineWidth = 0.8;
  // g.strokeStyle = 'white';
  // g.strokeText(str, x, y);
  // g.strokeStyle = 'black';
  // g.lineWidth = 1;
};

const get_shift = (k, date) => {
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  const days = days_diff(day, month, year);

  return get_shift_for_diff(k, days);
};

const get_shift_for_diff = (k, days) => {
  return (days >> 1) + k - 1 & 3;
};

const days_diff = (day, month, year) => {
  const initDate = [23, 5, 2021];

  const ms = 24 * 60 * 60 * 1e3;
  const d1 = Date.UTC(initDate[2], initDate[1] - 1, initDate[0]);
  const d2 = Date.UTC(year, month - 1, day);

  const days = (d2 - d1) / ms;
  assert(days === (days | 0));

  return days;
};

main();