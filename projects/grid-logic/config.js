"use strict"

const assert = require("assert")

const config = {
  allow_solving_partial_grid: 1,
}

module.exports = config