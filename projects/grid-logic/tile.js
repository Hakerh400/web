"use strict"

const assert = require("assert")
const Ent = require("./ent")

class Tile {
  constructor(grid, x, y, ent = null, val = null){
    this.grid = grid
    this.x = x
    this.y = y
    this.id = y * grid.w + x + 1
    this.ent = ent
    this.val = val
  }
  
  set_ent(ctor, dir){
    if(typeof ctor === "number")
      ctor = Ent.get_ctor_by_id(ctor)
    
    let ent = new ctor(this, dir)
    this.ent = ent
    this.grid.update()
    
    return ent
  }
  
  remove_ent(){
    this.ent = null
    this.grid.update()
  }
  
  clear(){
    this.remove_ent()
    this.val = null
  }
  
  nav(dir, cnt = 1){
    assert(cnt === (cnt | 0))
    assert(cnt >= 0)
    
    let {grid, x, y} = this
    let tile = this
    
    while(cnt !== 0){
      cnt--
      
      tile = grid.nav1(x, y, dir, 1)
      ;({x, y} = tile)
    }
    
    return tile
  }
}

module.exports = Tile