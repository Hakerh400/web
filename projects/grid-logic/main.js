"use strict"

const assert = require("assert")
const config = require("./config")
const Grid = require("./grid")
const Tile = require("./tile")
const Ent = require("./ent")

const {floor} = Math

const {
  ctors_num,
  EntId,
  EntId2,
  EntNot,
  EntOr,
  EntAnd,
} = Ent

const w = O.urlParam("w", "5") | 0
const h = O.urlParam("h", "5") | 0
const tile_size = 60
const sidebar_offset = 20
const sidebar_offset_v = sidebar_offset
const sidebar_offset_h = sidebar_offset

const sidebar_w = tile_size + sidebar_offset_h * 2
const sidebar_wh = sidebar_w / 2

const cols = {
  bg: "darkgray",
  sidebar: "#888",
  tile_bg: "#fff",
  tile_selected: "#dc5",
  val_0: "#4ff",
  val_1: "#f8f",
}

const sem = new O.Semaphore()

const {g} = O.ceCanvas(1)
const {canvas} = g

const sidebar_ent_dirs = []

let selected = null
let tile_hover = null

let iw, ih
let iwh, ihh

let cx = 0
let cy = 0
let cur_btn = null

let grid = null

const main = async () => {
  await sem.wait()
  
  for(let i = 0; i !== ctors_num; i++){
    let {dirs_num} = Ent.get_ctor_by_id(i)
    let dir = dirs_num === 4 ? 2 : 1
    sidebar_ent_dirs.push(dir)
  }
  
  init_grid()
  aels()
  on_resize()
  
  sem.signal()
}

const init_grid = () => {
  grid = new Grid(w, h)
}

const clear_grid = () => {
  grid.clear()
}

const gen_grid = async () => {
  grid.iter((x, y, d) => {
    d.ent = null
    
    let ctor_id = O.rand(5)
    let {dirs_num} = Ent.get_ctor_by_id(ctor_id)
    let dir = O.rand(dirs_num)
    
    d.set_ent(ctor_id, dir)
  })
  
  await grid.solve()
}

const set_size = (iw1, ih1) => {
  iw = iw1
  ih = ih1
  iwh = iw / 2
  ihh = ih / 2
}

const update_size = () => {
  set_size(O.iw, O.ih)
}

const with_size = (iw1, ih1, fn) => {
  let iw0 = iw
  let ih0 = ih
  
  set_size(iw1, ih1)
  fn()
  set_size(iw0, ih0)
}

const ael = (type, fn) => {
  O.ael(type, handle(fn))
}

const aels = () => {
  O.ael("contextmenu", on_context_menu)
  
  ael("resize", on_resize)
  ael("keydown", on_key_down)
  ael("mousedown", on_mouse_down)
  ael("mouseup", on_mouse_up)
  ael("mousemove", on_mouse_move)
  ael("wheel", on_wheel)
}

const handle = f => (...args) => {
  (async () => {
    await sem.wait()
    
    try{
      await f(...args)
    }finally{
      sem.signal()
    }
  })().catch(log)
}

const on_context_menu = evt => {
  O.pd(evt)
}

const on_resize = evt => {
  update_size()
  
  canvas.width = iw
  canvas.height = ih
  
  render()
}

const on_key_down = async evt => {
  let {code} = evt
  let flags = O.evtFlags(evt)
  
  if(flags === 0){
    if(code === "Enter"){
      await grid.solve()
      render()
      return
    }
    
    if(code === "Escape"){
      if(selected === null) return
      selected = null
      render()
      return
    }
    
    if(code === "KeyR"){
      selected = null
      clear_grid()
      render()
      return
    }
    
    if(code === "KeyC"){
      if(!grid.solved) return
      grid.unsolve()
      render()
      return
    }
    
    return
  }
  
  if(flags === 4){
    if(code === "KeyS"){
      O.pd(evt)
      
      let uri = export_to_uri()
      let link = O.doc.createElement("a")
      
      link.download = ""
      link.href = uri
      link.click()
      
      return
    }
    
    return
  }
}

const on_mouse_down = evt => {
  cur_btn = evt.button === 0 ? 0 :
    evt.button === 2 ? 1 : null
  process_cur(evt)
}

const on_mouse_up = evt => {
  cur_btn = null
  process_cur(evt)
}

const on_mouse_move = evt => {
  process_cur(evt)
}

const process_cur = (evt = null, do_render = 1) => {
  if(evt !== null) update_cur(evt)
  process_cur_btn()
  if(do_render) render()
}

const process_cur_btn = () => {
  let btn = cur_btn
  if(btn === null) return
  
  let info = cur_get_tile_info()
  
  if(info === null){
    if(cx < sidebar_w){
      if(selected === null) return
      
      selected = null
      render()
      
      return
    }
    
    return
  }
  
  let [type, x, y] = info
  
  // Sidebar
  if(type === 0){
    assert(x === 0)
    
    if(btn === 0){
      selected = [y, sidebar_ent_dirs[y]]
      render()
      return
    }
    
    return
  }
  
  // Grid
  if(type === 1){
    if(btn === 0){
      if(selected === null){
        let ent = grid.get_ent(x, y)
        if(ent === null) return
        
        selected = [ent.ctor_id, ent.dir]
        render()
        
        return
      }
      
      let [ctor_id, dir] = selected
      grid.set_ent(x, y, ctor_id, dir)
      render()
      
      return
    }
    
    if(btn === 1){
      grid.remove_ent(x, y)
      render()
      return
    }
    
    return
  }
  
  assert(0)
}

const on_wheel = evt => {
  if(selected === null) return
  
  let [ctor_id, dir] = selected
  let {dirs_num} = Ent.get_ctor_by_id(ctor_id)
  let d_dir = evt.deltaY > 1 ? 1 : -1
  let dir1 = (dir + d_dir + dirs_num) % dirs_num
  
  selected[1] = dir1
  render()
}

const update_cur = evt => {
  cx = evt.clientX
  cy = evt.clientY
}

const cur_get_tile_info = (cx0 = cx, cy0 = cy) => {
  let [gx, gy] = get_grid_coords()
  
  return cur_get_tile_info_for_grids(cx0, cy0, [
    [sidebar_offset_h, sidebar_offset_v, 1, ctors_num],
    [gx, gy, w, h],
  ])
}

const cur_get_tile_info_for_grids = (cx0 = cx, cy0 = cy, grids) => {
  for(let i = 0; i !== grids.length; i++){
    let [x1, y1, w, h] = grids[i]
    let x2 = x1 + w * tile_size
    let y2 = y1 + h * tile_size
    
    if(cx < x1 || cx >= x2) continue
    if(cy < y1 || cy >= y2) continue
    
    let x = floor((cx - x1) / tile_size)
    let y = floor((cy - y1) / tile_size)
    
    return [i, x, y]
  }
  
  return null
}

const get_grid_coords = () => {
  return [
    iwh + sidebar_wh - w / 2 * tile_size,
    ihh - h / 2 * tile_size,
  ]
}

const render = () => {
  render_g(g)
}

const render_g = g => {
  g.fillStyle = cols.bg
  g.fillRect(0, 0, iw, ih)
  
  draw_sidebar(g)
  draw_grid(g)
  draw_selected(g)
}

const draw_sidebar = g => {
  g.fillStyle = cols.sidebar
  g.beginPath()
  g.rect(-2, -2, sidebar_w + 2, ih + 4)
  g.fill()
  g.stroke()
  
  g.save()
  g.translate(sidebar_offset_h, sidebar_offset_v)
  g.scale(tile_size)
  
  for(let i = 0; i !== ctors_num; i++){
    let ctor = Ent.get_ctor_by_id(i)
    
    g.save()
    g.translate(0, i)
    
    g.fillStyle = selected !== null && i === selected[0] ?
      cols.tile_selected : cols.tile_bg
    
    g.beginPath()
    g.rect(0, 0, 1, 1)
    g.fill()
    g.stroke()
    
    let dir = sidebar_ent_dirs[i]
    let ent = new ctor(null, dir)
    ent.render(g)
    
    g.restore()
  }
  
  g.restore()
}

const draw_grid_aux = g => {
  const {gs} = g
  
  grid.iter((x, y, d) => {
    g.save()
    g.translate(x, y)
    
    g.fillStyle = !grid.solved || d.val === null ?
      cols.tile_bg : d.val ? cols.val_1 : cols.val_0
    g.fillRect(0, 0, 1, 1)
    
    if(d.ent) d.ent.render(g)
    
    g.restore()
  })
  
  g.beginPath()
  for(let y = 0; y <= h; y++){
    g.moveTo(0, y)
    g.lineTo(w + gs, y)
  }
  for(let x = 0; x <= w; x++){
    g.moveTo(x, 0)
    g.lineTo(x, h + gs)
  }
  g.stroke()
}

const draw_grid = g => {
  g.save()
  g.translate(iwh + sidebar_wh, ihh)
  g.scale(tile_size)
  g.translate(-w / 2, -h / 2)
  draw_grid_aux(g)
  g.restore()
}

const draw_selected = g => {
  if(selected === null) return
  let [ctor_id, dir] = selected
  
  let info = cur_get_tile_info()
  if(info === null) return
  
  let [type, x, y] = info
  if(type !== 1) return
  
  let [gx, gy] = get_grid_coords()
  
  g.save()
  g.globalAlpha = .5
  
  g.translate(gx, gy)
  g.scale(tile_size)
  g.translate(x, y)
  
  let ctor = Ent.get_ctor_by_id(ctor_id)
  let ent = new ctor(null, dir)
  ent.render(g)
  
  g.globalAlpha = 1
  g.restore()
}

const export_to_uri = () => {
  let iw = w * tile_size + 1
  let ih = h * tile_size + 1
  let canvas = O.doc.createElement("canvas")
  
  canvas.width = iw
  canvas.height = ih
  
  let g0 = canvas.getContext("2d")
  let g = new O.EnhancedRenderingContext(g0)
  
  with_size(iw, ih, () => {
    g.scale(tile_size)
    draw_grid_aux(g)
  })
  
  return canvas.toDataURL()
}

main().catch(log)