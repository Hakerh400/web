"use strict"

const assert = require("assert")

class Ent {
  static get ctor_id(){ O.virtual("ctor_id") }
  
  constructor(tile){
    this.tile = tile
  }
  
  get grid(){ return this.tile.grid }
  get x(){ return this.tile.x }
  get y(){ return this.tile.y }
  get id(){ return this.tile.id }
  get ctor_id(){ return this.constructor.ctor_id }
  
  nav(dir, cnt){
    return this.tile.nav(dir, cnt)
  }
  
  get_cnd(){ O.virtual("get_cnd") }
  render(g){ O.virtual("render") }
}

class EntR extends Ent {
  static get dirs_num(){ O.virtual("dirs_num") }
  
  constructor(tile, dir){
    super(tile)
    
    assert(dir === (dir | 0))
    assert(dir >= 0 && dir < this.dirs_num)
    
    this.dir = dir
  }
  
  get dirs_num(){ return this.constructor.dirs_num }
  
  render(g){
    g.save(1)
    g.rotate(.5, .5, -this.dir * O.pih)
    this.render_rot(g)
    g.restore()
  }
}

class EntR2 extends EntR {
  static get dirs_num(){ return 2 }
}

class EntR4 extends EntR {
  static get dirs_num(){ return 4 }
}

class EntId extends EntR4 {
  static get ctor_id(){ return 0 }
  
  get_cnd(){
    let a = this.id
    let b = this.nav(this.dir).id
    return [[a, -b], [-a, b]]
  }
  
  render_rot(g){
    g.beginPath()
    g.moveTo(.25, .25)
    g.lineTo(.5, .75)
    g.lineTo(.75, .25)
    g.stroke()
  }
}

class EntId2 extends EntR4 {
  static get ctor_id(){ return 1 }
  
  get_cnd(){
    let a = this.id
    let b = this.nav(this.dir, 2).id
    return [[a, -b], [-a, b]]
  }
  
  render_rot(g){
    g.beginPath()
    for(let i = 0; i !== 2; i++){
      let dy = i * 0.2
      let dx = dy / 2
      g.moveTo(.25 + dx, .25)
      g.lineTo(.5, .75 - dy)
      g.lineTo(.75 - dx, .25)
    }
    g.stroke()
  }
}

class EntNot extends EntR4 {
  static get ctor_id(){ return 2 }
  
  get_cnd(){
    let a = this.id
    let b = this.nav(this.dir).id
    return [[a, b], [-a, -b]]
  }
  
  render_rot(g){
    g.beginPath()
    g.moveTo(.25, .75)
    g.lineTo(.75, .75)
    g.moveTo(.5, .25)
    g.lineTo(.5, .75)
    g.stroke()
  }
}

class EntOr extends EntR2 {
  static get ctor_id(){ return 3 }
  
  get_cnd(){
    let a = this.id
    let b = this.nav(this.dir).id
    let c = this.nav(this.dir + 2 & 3).id
    return [
      [a, -b],
      [a, -c],
      [-a, b, c],
    ]
  }
  
  render_rot(g){
    g.beginPath()
    g.moveTo(.5, .15)
    g.lineTo(.5, .85)
    g.stroke()
  }
  
  render(g){
    super.render(g)
    g.beginPath()
    g.arc(.5, .5, .2, 0, O.pi2);
    g.stroke()
  }
}

class EntAnd extends EntR2 {
  static get ctor_id(){ return 4 }
  
  get_cnd(){
    let a = this.id
    let b = this.nav(this.dir).id
    let c = this.nav(this.dir + 2 & 3).id
    return [
      [-a, b],
      [-a, c],
      [a, -b, -c],
    ]
  }
  
  render_rot(g){
    g.beginPath()
    g.moveTo(.5, .15)
    g.lineTo(.5, .85)
    g.stroke()
  }
}

const ctors_list = [
  EntId,
  EntId2,
  EntNot,
  EntOr,
  EntAnd,
]

const ctors_num = ctors_list.length

const get_ctor_by_id = i => {
  assert(i === (i | 0))
  assert(i >= 0 && i < ctors_num)
  return ctors_list[i]
}

module.exports = Object.assign(Ent, {
  ctors_list,
  ctors_num,
  get_ctor_by_id,
  
  EntR,
  EntR2,
  EntR4,
  EntId,
  EntId2,
  EntNot,
  EntOr,
  EntAnd,
})