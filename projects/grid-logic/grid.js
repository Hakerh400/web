"use strict"

const assert = require("assert")
const config = require("./config")
const Tile = require("./tile")

class Grid extends O.Grid {
  solved = 0
  
  constructor(w, h, fn = null){
    super(w, h)
    
    this.iter((x, y) => {
      this.set(x, y, new Tile(this, x, y))
    })
    
    if(fn !== null)
      this.iter(fn)
  }
  
  get_ent(x, y){
    return this.get(x, y).ent
  }
  
  set_ent(x, y, ctor, dir){
    return this.get(x, y).set_ent(ctor, dir)
  }
  
  remove_ent(x, y){
    this.get(x, y).remove_ent()
  }
  
  clear_tile(x, y){
    this.get(x, y).clear()
  }
  
  clear(){
    this.iter((x, y, d) => {
      d.clear()
    })
  }
  
  update(){
    this.unsolve()
  }
  
  async solve(){
    this.solved = 0
    
    let has_emp = 0
    
    this.iter((x, y, d) => {
      if(d.ent === null) has_emp = 1
    })
    
    if(has_emp && !config.allow_solving_partial_grid)
      return
    
    let cnf = []
    
    this.iter((x, y, d) => {
      let {ent} = d
      if(ent === null) return
      
      for(let c of ent.get_cnd())
        cnf.push(c)
    })
    
    let sol = await O.rmi("sat.solve", cnf)
    if(sol === null) return
    
    this.iter((x, y, d) => {
      d.val = sol[d.id - 1]
    })
    
    this.solved = 1
  }
  
  unsolve(){
    this.solved = 0
  }
}

module.exports = Grid