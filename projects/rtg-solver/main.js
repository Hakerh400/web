'use strict';

const assert = require('assert');
const Grid = require('./grid');
const Tile = require('./tile');

const {abs, min, max} = Math;
const {pi, pi2} = O;

const w = 24;
const h = 12;
const size = 60;
const bodyW1 = .5;
const cactusRad = .4;
const goalRad = .35;

const bodyW = bodyW1 * size;
const bodyWH = bodyW / 2;
const bodyWS = bodyW / size;
const bodyWHS = bodyWS / 2;

const cols = {
  canvasBg: 'darkgray',
  floor: 'rgb(255,178,94)',
  wall: 'rgb(177,84,38)',
  gridLines: 'rgba(0,0,0,.2)',
  pathLines: 'rgb(137,88,56)',
  body: 'rgb(165,186,41)',
  head: 'rgb(181,230,29)',
  cactus: 'rgb(56,138,0)',
  goal: 'rgba(0,0,0,.5)',
};

const {g} = O.ceCanvas(1);
const {canvas} = g;

const grid = new Grid(w, h);
const {head, cactus, goal} = grid;
const history = [];

let iw, ih;
let iwh, ihh;

const main = () => {
  initGrid();
  
  reset();
  aels();
  onResize();
};

const initGrid = () => {
  const vlines = [2, 7, 9, 11, 13, 15, 16, 18, 20, 22];
  
  grid.iter((x, y, d) => {
    if(x <= 2 || x >= w - 3 || y <= 1 || y >= h - 2)
      d.wall = 1;
    
    if(y >= 3 && y <= h - 3 && x >= 2 && x <= w - 3)
      d.pthH = 1;
    
    if(y >= 3 && y <= h - 4 && vlines.includes(x))
      d.pthV = 1;
  });
  
  head[0] = 7
  head[1] = 5;
  cactus[0] = 14;
  cactus[1] = 5;
  goal[0] = 11;
  goal[1] = 4;
  
  history.push(grid.save());
};

const load = () => {
  grid.load(O.last(history));
  render();
};

const reset = () => {
  history.length = 1;
  load();
};

const undo = () => {
  if(history.length === 1) return;
  history.pop();
  load();
};

const play = dir => {
  if(!move(dir)) return;
  
  history.push(grid.save());
  render();
};

const solve = async () => {
  const games = [O.last(history)];
  const dirs = [0, 1, 2, 3].reverse();
  let n = 0;
  
  while(games.length !== 0){
    if(n++ === 1e4){
      n = 0;
      render();
      await O.rafa2(O.nop);
    }
    
    const info = games.pop();
    
    for(const dir of O.shuffle(dirs)){
      grid.load(info);
      if(!move(dir)) continue;
      
      if(grid.won){
        render();
        return 1;
      }
      
      games.push(grid.save());
    }
  }
  
  render();
  return 0;
};

const move = dir => {
  let dx = 0;
  let dy = 0;
  
  switch(dir){
    case 0: dy = -1; break;
    case 1: dx = 1; break;
    case 2: dy = 1; break;
    case 3: dx = -1; break;
  }
  
  const ver = dy !== 0;
  const hor = dx !== 0;
  const [cx, cy] = cactus;
  const tiles = [];
  
  let [xPrev, yPrev] = head;
  let pushedCactusWithHead = 0;
  let pushedCactusWithBody = 0;
  let x, y;
  
  while(1){
    x = xPrev + dx;
    y = yPrev + dy;
    
    if(x === cx && y === cy)
      pushedCactusWithHead = 1;
    
    const xx = min(xPrev, x);
    const yy = min(yPrev, y);
    const d = grid.get(x, y);
    
    let joint = 0;
    
    for(const d1 of d.adjs()){
      const x1 = d1.x;
      const y1 = d1.y;
      
      if(y1 === y && d1.bodyH) return 0;
      if(x1 === x && d1.bodyV) return 0;
      
      if(ver && y1 === y && d1.pthH) joint = 1;
      if(hor && x1 === x && d1.pthV) joint = 1;
    }
    
    const dd = grid.get(xx, yy);
    
    if(ver) if(!dd.pthV) return 0;
    if(hor) if(!dd.pthH) return 0;
    
    tiles.push(dd);
    
    if(joint) break;
    
    xPrev = x;
    yPrev = y;
  }
  
  const d = O.last(tiles);
  const leverage = d.hasWall();
  const n = tiles.length;
  const dxn = dx * n;
  const dyn = dy * n;
  const cx1 = cx + (leverage ? -dxn : dxn);
  const cy1 = cy + (leverage ? -dyn : dyn);
  
  const isCactusDestroyed = () => {
    const dc1 = grid.get(cx1, cy1);
    if(dc1 === null) return 1;
    
    if(dc1.hasWall()) return 1;
    if(dc1.hasBody(pushedCactusWithBody)) return 1;
    
    return 0;
  };
  
  if(pushedCactusWithHead){
    if(leverage) return 0;
    if(isCactusDestroyed()) return 0;
  }
  
  if(leverage){
    let hasSpace = 1;
    
    grid.iter((x, y, d) => {
      d.bodyH1 = 0;
      d.bodyV1 = 0;
    });
    
    grid.iter((x, y, d) => {
      if(!hasSpace) return;
      
      const {bodyH, bodyV} = d;
      if(!(bodyH || bodyV)) return;
      
      const x1 = x - dxn;
      const y1 = y - dyn;
      const d1 = grid.get(x1, y1);
      const ps = [[x1, y1]];
      
      if(bodyH) ps.push([x1 + 1, y1]);
      if(bodyV) ps.push([x1, y1 + 1]);
      
      for(const [x1, y1] of ps){
        const d = grid.get(x1, y1);
        
        if(d.hasWall()){
          hasSpace = 0;
          return;
        }
        
        if(!pushedCactusWithBody){
          const xa = min(x, x1);
          const ya = min(y, y1);
          const xb = max(x, x1);
          const yb = max(y, y1);
          
          if(hor && cy === yb && cx >= xa && cx <= xb) pushedCactusWithBody = 1;
          if(ver && cx === xb && cy >= ya && cy <= yb) pushedCactusWithBody = 1;
        }
      }
      
      d1.bodyH1 = bodyH;
      d1.bodyV1 = bodyV;
    });
    
    if(!hasSpace) return 0;
    if(pushedCactusWithBody && isCactusDestroyed()) return 0;
    
    grid.iter((x, y, d) => {
      d.bodyH = d.bodyH1;
      d.bodyV = d.bodyV1;
    });
    
    for(let i = 0; i !== n; i++){
      const {x, y} = tiles[i];
      tiles[i] = grid.get(x - dxn, y - dyn);
    }
  }else{
    head[0] = x;
    head[1] = y;
  }
  
  if(ver) for(const d of tiles) d.bodyV = 1;
  if(hor) for(const d of tiles) d.bodyH = 1;
  
  if(pushedCactusWithHead || pushedCactusWithBody){
    cactus[0] = cx1;
    cactus[1] = cy1;
  }
  
  return 1;
};

const aels = () => {
  O.ael('keydown', onKeyDown);
  O.ael('resize', onResize);
};

const onKeyDown = evt => {
  const {code} = evt;
  
  switch(code){
    case 'ArrowUp': play(0); break;
    case 'ArrowRight': play(1); break;
    case 'ArrowDown': play(2); break;
    case 'ArrowLeft': play(3); break;
    case 'KeyR': location.reload(); break;
    case 'KeyZ': undo(); break;
    case 'KeyS': solve(); break;
  }
};

const onResize = evt => {
  iw = O.iw;
  ih = O.ih;
  iwh = iw / 2;
  ihh = ih / 2;
  
  canvas.width = iw;
  canvas.height = ih;
  
  g.lineCap = 'square';
  g.lineJoin = 'miter';
  
  render();
};

const render = () => {
  g.fillStyle = 'darkgray';
  g.fillRect(0, 0, iw, ih);
  
  g.translate(iwh, ihh);
  g.scale(size);
  g.translate(-w / 2, -h / 2);
  
  const {gs} = g;
  const gs2 = gs * 2;
  const gs4 = gs * 4;
  
  grid.iter((x, y, d) => {
    g.save();
    g.translate(x, y);
    
    g.fillStyle = d.wall ? cols.wall : cols.floor;
    g.fillRect(0, 0, 1, 1);
    
    g.restore();
  });
  
  g.strokeStyle = cols.gridLines;
  g.beginPath();
  for(let y = 0; y <= h; y++){
    g.moveTo(0, y);
    g.lineTo(w + gs, y);
  }
  for(let x = 0; x <= w; x++){
    g.moveTo(x, 0);
    g.lineTo(x, h + gs);
  }
  g.stroke();
  
  g.globalCompositeOperation = 'darken';
  g.lineWidth = 5;
  g.fillStyle = 'black';
  grid.iter((x, y, d) => {
    const {pthH, pthV, bodyH, bodyV} = d;
    
    g.save();
    g.translate(x, y);
    
    if(pthH || pthV){
      g.strokeStyle = d.wall ? 'black' : cols.pathLines;
      g.beginPath();
      
      if(pthH){
        g.moveTo(gs, 0);
        g.lineTo(1 - gs, 0);
      }
      
      if(pthV){
        g.moveTo(0, gs);
        g.lineTo(0, 1 - gs);
      }
      
      g.stroke();
    }
    
    if(bodyH || bodyV){
      if(bodyH) g.fillRect(-bodyWHS - gs2, -bodyWHS - gs2, 1 + bodyWS + gs4, bodyWS + gs4);
      if(bodyV) g.fillRect(-bodyWHS - gs2, -bodyWHS - gs2, bodyWS + gs4, 1 + bodyWS + gs4);
    }
    
    g.restore();
  });
  g.strokeStyle = 'black';
  g.globalCompositeOperation = 'source-over';
  
  g.lineWidth = 2;
  g.fillStyle = cols.goal;
  g.beginPath();
  g.arc(...goal, goalRad, 0, pi2);
  g.stroke();
  g.fill();
  
  g.fillStyle = cols.cactus;
  g.beginPath();
  g.arc(...cactus, cactusRad, 0, pi2);
  g.stroke();
  g.fill();
  
  g.strokeStyle = cols.body;
  g.lineWidth = bodyW;
  grid.iter((x, y, d) => {
    const {bodyH, bodyV} = d;
    if(!(bodyH || bodyV)) return;
    
    g.save();
    g.translate(x, y);
    
    g.beginPath();
    if(bodyH){
      g.moveTo(gs, 0);
      g.lineTo(1 - gs, 0);
    }
    if(bodyV){
      g.moveTo(0, gs);
      g.lineTo(0, 1 - gs);
    }
    g.stroke();
    
    g.restore();
  });
  g.strokeStyle = 'black';
  
  g.lineWidth = 2;
  g.fillStyle = cols.head;
  g.beginPath();
  g.arc(...head, bodyWHS, 0, pi2);
  g.stroke();
  g.fill();
  
  g.lineWidth = 1;
  g.beginPath();
  g.rect(0, 0, w, h);
  g.stroke();
  
  g.resetTransform();
};

main();