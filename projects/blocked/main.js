'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');

const cwd = __dirname;
const cssFile = path.join(cwd, 'style.css');

await O.addStyle(cssFile, 0);

const main = () => {
  
  const div = O.ceDiv(O.body, 'msg');
  div.innerText = 'Blocked';
};

main();