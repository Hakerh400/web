"use strict"

const fs = require("fs")
const path = require("path")
const assert = require("assert")
const msgs_src = require("./msgs.txt")

const {min, max} = Math
const {pi, pih, pi2} = O

const iters_num = 5e3

const font_size = O.urlParam("s", 150) | 0

const cwd = __dirname
const inp_file = path.join("../../../Other/io/out.txt")

let canvas = null
let g = null
let iw = null
let ih = null
let iwh = null
let ihh = null

let msgs = null
let position = null

const main = async () => {
  msgs = O.sanl(msgs_src)
  
  let get_col = (() => {
    let cols = [
      [0, [0, 7, 100]],
      [0.16, [32, 107, 203]],
      [0.42, [237, 255, 255]],
      [0.6425, [255, 170, 0]],
      // [0.8575, [0, 2, 0]],
    ]
    
    let mx = cols.reduce((a, b) => max(a, b[0]), 0)
    
    for(let col of cols)
      col[0] /= mx
    
    var createInterpolant = (()=>{
      var j = 0;
      return function(xs, ys) {
        var i, length = xs.length;
        if (length != ys.length) { throw 'Need an equal count of xs and ys.'; }
        if (length === 0) { return function(x) { return 0; }; }
        if (length === 1) {
            var result = +ys[0];
            return function(x) { return result; };
        }
        var indexes = [];
        for (i = 0; i < length; i++) { indexes.push(i); }
        indexes.sort(function(a, b) { return xs[a] < xs[b] ? -1 : 1; });
        var oldXs = xs, oldYs = ys;
        xs = []; ys = [];
        for (i = 0; i < length; i++) { xs.push(+oldXs[indexes[i]]); ys.push(+oldYs[indexes[i]]); }
        var dys = [], dxs = [], ms = [];
        for (i = 0; i < length - 1; i++) {
            var dx = xs[i + 1] - xs[i], dy = ys[i + 1] - ys[i];
            dxs.push(dx); dys.push(dy); ms.push(dy/dx);
        }
        var c1s = [ms[0]];
        for (i = 0; i < dxs.length - 1; i++) {
            var m = ms[i], mNext = ms[i + 1];
            if (m*mNext <= 0) {
                c1s.push(0);
            } else {
                var dx_ = dxs[i], dxNext = dxs[i + 1], common = dx_ + dxNext;
                c1s.push(3*common/((common + dxNext)/m + (common + dx_)/mNext));
            }
        }
        c1s.push(ms[ms.length - 1]);
        var c2s = [], c3s = [];
        for (i = 0; i < c1s.length - 1; i++) {
            var c1 = c1s[i];
            var m_ = ms[i];
            var invDx = 1/dxs[i];
            var common_ = c1 + c1s[i + 1] - m_ - m_;
            c2s.push((m_ - c1 - common_)*invDx);
            c3s.push(common_*invDx*invDx);
        }
        return function(x) {
            var i = xs.length - 1;
            var low = 0, mid, high = c3s.length - 1, rval, dval;
            while (low <= high) {
                mid = Math.floor(0.5*(low + high));
                var xHere = xs[mid];
                if (xHere < x) { low = mid + 1; }
                else if (xHere > x) { high = mid - 1; }
                else {
                    j++;
                    i = mid;
                    var diff = x - xs[i];
                    rval = ys[i] + diff * (c1s[i] + diff *  (c2s[i] + diff * c3s[i]));
                    dval = c1s[i] + diff * (2*c2s[i] + diff * 3*c3s[i]);
                    return [ rval, dval ];
                }
            }
            i = Math.max(0, high);
            var diff = x - xs[i];
            j++;
            rval = ys[i] + diff * (c1s[i] + diff *  (c2s[i] + diff * c3s[i]));
            dval = c1s[i] + diff * (2*c2s[i] + diff * 3*c3s[i]);
            return [ rval, dval ];
        };
    }
  })();
    
    let fs = O.ca(3, c => {
      return createInterpolant(
        cols.map(a => a[0]),
        cols.map(a => a[1][c]),
      )
    })
    
    let f = x => fs.map(f => f(x)[0])
    
    return x => (new O.Color(...f(x))).toString()
  })()
  
  O.body.style.backgroundColor = get_col(0)
  
  // await O.wait_for_enter()
  
  init_canvas()
  
  // {
  //   for(let x = 0; x !== iw; x++){
  //     g.fillStyle = get_col(x / iw)
  //     g.fillRect(x, 0, 1, ih)
  //   }
  // 
  //   return
  // }
  
  let src = await O.rfs(inp_file, 1)
  
  let mx = 0
  let sum = 0
  
  let xs = O.sanl(src).map(line => {
    let [s, n] = line.split(/\s+/)
    
    n = Number(n)
    mx = max(mx, n)
    sum += n
    
    return [s, n]
  })
  
  let xs_num = xs.length
  
  g.textBaseline = "top"
  g.textAlign = "left"
  
  for(let i = 0; i !== xs_num; i++){
    let [s, n] = xs[i]
    let fst = i === 0
    
    s = s.toUpperCase()
    
    if((i + 1) % 10 === 0)
      await O.rafa2()
    
    let k = n / mx
    let k1 = k ** 0.5
    let font = font_size * k1
    
    g.font = `${font}px arial`
    
    let h = font / 117 * 100 | 0
    let w = O.str_w(g, s)
    
    let mn = 0.005
    let col = get_col(mn + k1 * (1 - mn))
    
    position = null
    if(i < 5) position = 0
    
    with_avail_rect(w, h, fst, () => {
      g.fillStyle = col
      g.fillText(s, 0, 0)
    })
  }
  
  log(LS(2))
}

const init_canvas = () => {
  g = O.ceCanvas().g
  canvas = g.canvas
  iw = O.iw
  ih = O.ih
  iwh = iw / 2
  ihh = ih / 2
  
  g.clearRect(0, 0, iw, ih)
}

const find_avail_rect = (occupied, w, h) => {
  let md = null
  let mx = null
  let my = null
  
  let wh = w >> 1
  let hh = h >> 1
  let x_ub = iw - w | 0
  let y_ub = ih - h | 0
  
  iter_loop: for(let iter = 0; iter !== iters_num; iter++){
    let x = O.rand(x_ub)
    let y = O.rand(y_ub)
    
    for(let j = 0; j !== h; j++)
      for(let i = 0; i !== w; i++)
        if(occupied(x + i, y + j))
          continue iter_loop
    
    let d = O.dists(x + wh, y + hh, iwh, ihh)
    
    if(md === null || d < md){
      md = d
      mx = x
      my = y
    }
  }
  
  if(md === null) return null
  return [mx, my]
}

const with_avail_rect = (...args) => {
  let args_num = args.length
  
  if(args_num === 3)
    args.splice(2, 0, 0)
  
  let [w, h, fst, fn] = args
  
  w |= 0
  h |= 0
  
  if(w === 0 || h === 0) return
  
  let imgd = g.getImageData(0, 0, iw, ih)
  let {data} = imgd
  
  let occupied = (x, y) => {
    return data[((y | 0) * (iw | 0) + (x | 0) << 2) + 3] !== 0
  }
  
  const invoke_fn = (x, y, rot) => {
    g.save()
    g.translate(x, y)
    
    if(rot){
      g.rotate(-pih)
      g.translate(-w, 0)
    }
    
    fn()
    g.restore()
  }
  
  if(fst){
    let x = iw - w >> 1
    let y = ih - h >> 1
    let rot = 0
    
    invoke_fn(x, y, rot)
    return
  }
  
  let rots = O.shuffle([0, 1])
  
  for(let rot of rots){
    if(position !== null && rot !== position)
      continue
    
    let w1 = rot ? h : w
    let h1 = rot ? w : h
    let dx = rot ? -w : 0
    
    let rect = find_avail_rect(occupied, w1, h1)
    if(rect === null) continue
    
    let [x, y] = rect
    invoke_fn(x, y, rot)
    
    return
  }
  
  err(1)
}

const err = i => {
  throw new Error(LS(i))
}

const LS = i => {
  assert(i === (i | 0))
  assert(i >= 1)
  assert(i <= msgs.length)
  
  return msgs[i - 1]
}

const on_err = err => {
  log(err)
}

main().catch(on_err)