'use strict';

const fs = require('fs');
const path = require('path');

const TIME_INCREMENT = 5;

const cwd = __dirname;

var format = 'mp4';
var videoName = getVideoName();

// var videoFile = path.join(cwd, `${videoName}.${format}`);
var videoFile = `/Render/${videoName}.${format}`;

var fsc = O.urlParam('fs', '1') === '1';

var video = null;
var source = null;
var loaded = false;

window.setTimeout(main);

function main(){
  aels();

  O.body.style.margin = '0px';
  O.body.style.backgroundColor = 'black';
  O.body.style.overflow = 'hidden';

  video = O.ce(O.body, 'video');
  video.style.position = 'absolute';
  video.style.top = '50%';
  video.style.left = '50%';
  video.style.transform = 'translate(-50%, -50%)';
  // video.style.imageRendering = 'pixelated';
  video.loop = false;

  if(fsc){
    video.style.width = '100%';
    video.style.height = '100%';
  }

  O.ael(video, 'canplay', () => {
    loaded = true;
  });
  
  source = O.ce(video, 'source');
  source.src = O.urlTime(videoFile);
}

function aels(){
  O.ael('keydown', evt => {
    if(!loaded) return;

    switch(evt.code){
      case 'Space':
        if(video.paused) video.play();
        else video.pause();
        break;

      case 'ArrowLeft':
        video.currentTime -= TIME_INCREMENT;
        break;

      case 'ArrowRight':
        video.currentTime += TIME_INCREMENT;
        break;

      case 'F5':
        if(evt.ctrlKey) break;
        evt.preventDefault();
        if(!video.paused) video.pause();
        video.controls = 0;
        loaded = false;
        source.src = O.urlTime(videoFile);
        video.load();
        break;
      
      case 'KeyQ':
        video.controls ^= 1;
        break;
    }
  });
}

function getVideoName(){
  var name = O.urlParam('name');
  if(name === null)
    return '1';

  return name;
}