"use strict"

const assert = require("assert")

const {
  min, max, abs, floor,
  ceil, round, sqrt, sin, cos,
  tan, atan, atan2, tanh,
  log2, clz32,
} = Math

const {pi, pih, pi2} = O

// Circle radius scaled by `min(width, height)` of the canvas
const radius_factor = 0.9

// The number of circles drawn, must be odd
// This is one more than the number of visible vertical tiles
const n = 101

// The scale factor of circles
const p1 = 20

let canvas = null
let g = null
let iw = null
let ih = null
let iwh = null
let ihh = null
let rad = null
let diam = null

const cols = {
  bg: "darkgray",
  tile_bg: "#fff",
}

const main = () => {
  assert((n & 1) === 1)
  
  g = O.ceCanvas().g
  canvas = g.canvas
  
  aels()
  on_resize()
}

const aels = () => {
  O.ael("resize", on_resize)
}

const on_resize = () => {
  if(iw !== null)
    g.clearRect(0, 0, iw, ih)
  
  iw = O.iw
  ih = O.ih
  iwh = iw / 2
  ihh = ih / 2
  
  rad = min(iwh, ihh) * radius_factor
  diam = rad * 2
  
  canvas.width = iw
  canvas.height = ih
  
  render()
}

const render = () => {
  g.fillStyle = cols.bg
  g.fillRect(0, 0, iw, ih)
  
  g.lineWidth = 1 / rad
  g.translate(iwh, ihh)
  g.scale(rad, rad)
  
  g.fillStyle = cols.tile_bg
  g.beginPath()
  g.arc(0, 0, 1, 0, pi2)
  g.fill()
  g.stroke()
  
  g.save()
  g.clip()
  
  const n = 11
  const p1 = 2
  
  const f1 = tanh(0.5 * p1)
  const f2 = tanh(-0.5 * p1)
  
  const calc_r = y => {
    let k0 = (y + 1) / n
    let k = (tanh(p1 * k0 - 0.5 * p1) - f2) / (f1 - f2)
    
    return k
  }
  
  let r_prev = null
  let cy_prev = null
  
  for(let y = 0; y <= n; y++){
    let r = calc_r(y)
    let d = r * 2
    let cy = r - 1
    
    g.beginPath()
    g.arc(0, cy, r, 0, pi2)
    g.stroke()
    
    if(y !== 0 && y !== n){
      let tiles_num = min(2 ** y + 1, 2e3)
      let lines_num = tiles_num - 1
      
      g.beginPath()
      
      for(let x = 0; x !== lines_num; x++){
        let p1 = 5
        let k0 = (x + 1) / (lines_num + 1)
        let k = (tanh(p1 * k0 - 0.5 * p1) - f2) / (f1 - f2)
        
        let a = pi2 * (k - 0.25)
        
        let x1 = cos(a) * r
        let y1 = cy + sin(a) * r
        
        let R = r_prev
        let C = cy
        let P = cy_prev
        let A = C - P
        let t = tan(a)
        
        let a1 = 1 + t * t
        let b1 = 2 * t * A
        let c1 = A * A - R * R
        
        let m1 = -b1
        let m2 = sqrt(b1 * b1 - 4 * a1 * c1)
        let m3 = 2 * a1
        
        let sx1 = (m1 + m2) / m3
        let sx2 = (m1 - m2) / m3
        let sy1 = t * sx1 + C
        let sy2 = t * sx2 + C
        
        let dist1 = O.dist(x1, y1, sx1, sy1)
        let dist2 = O.dist(x1, y1, sx2, sy2)
        
        let x2, y2
        
        if(dist1 < dist2){
          x2 = sx1
          y2 = sy1
        }else{
          x2 = sx2
          y2 = sy2
        }
        
        g.moveTo(x1, y1)
        g.lineTo(x2, y2)
      }
      
      g.stroke()
    }
    
    r_prev = r
    cy_prev = cy
  }
  
  g.fillStyle = "red"
  g.beginPath()
  g.arc(0, 0, 0.01, 0, pi2)
  g.fill()
  
  g.restore()
  g.resetTransform()
  g.lineWidth = 1
}

main()