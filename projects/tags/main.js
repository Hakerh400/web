'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');

const base_url = '/Other/P';

await O.addStyle('style.css');

const div = O.ce(O.body, 'div');

let inp_pth = null;
let inp_ta = null;

const main = async () => {
  await main_menu();
};

const main_menu = async () => {
  div.innerText = '';
  
  inp_pth = null;
  inp_ta = null;
  
  const [n, k] = await O.rmi('tags.get_status');
  
  const btn_search = O.ceBtn(div, 'Search', '', on_btn_search);
  const btn_add_tags = O.ceBtn(div, 'Add tags', '', on_btn_add_tags);
  if(k === n) btn_add_tags.disabled = 1;
  
  const pb_table = O.ce(div, 'table');
  pb_table.classList.add('progress-table');
  
  const tbody = O.ce(pb_table, 'tbody');
  let tr = O.ce(tbody, 'tr');
  
  let td = O.ce(tr, 'td');
  const pb = O.ce(td, 'progress');
  pb.max = n;
  pb.value = k;
  
  td = O.ce(tr, 'td');
  const pb_info = O.ceDiv(td, 'progress-info');
  pb_info.innerText = `${k}/${n} (${show_percent(n, k)})`;
  
  // await O.rmi('tags.add_tags', String(O.now), ['abc', 'de', 'xy']);
};

const on_btn_search = async () => {
  log('search');
};

const on_btn_add_tags = async () => {
  await next_file();
};

const next_file = async () => {
  div.innerText = '';
  
  const pth = await O.rmi('tags.get_next_file');
  if(pth === null) return main_menu();
  
  inp_pth = O.ceInput(div, 'text', 'input-text');
  inp_pth.value = pth;
  
  inp_ta = O.ce(div, 'textarea');
  inp_ta.classList.add('input-ta');
  
  O.ael(inp_ta, 'keydown', async evt => {
    if(evt.code === 'Enter'){
      add_tags(1);
      return;
    }
  });
  
  const btn_add = O.ceBtn(div, 'Add', '', on_btn_add);
  
  const a = O.doc.createElement('a');
  a.target = '_blank';
  a.href = path.join(base_url, pth);
  a.click();
  
  inp_ta.focus();
};

const on_btn_add = async () => {
  await add_tags(0);
};

const add_tags = async (next=0) => {
  const pth = inp_pth.value;
  const tags = inp_ta.value.trim().split(' ');
  await O.rmi('tags.add_tags', pth, tags);
  
  if(next){
    await next_file();
    return;
  }
  
  await main_menu();
};

const show_percent = (n, k) => {
  return O.percent(k, n + 1);
};

main().catch(log);