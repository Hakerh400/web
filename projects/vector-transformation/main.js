'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');

const {
  min, max, abs, floor,
  ceil, round, sqrt, sin, cos,
  acos, atan, atan2,
  log2,
} = Math;

const {pi, pih} = O;

const {g} = O.ceCanvas();
const {canvas} = g;

let iw, ih;
let iwh, ihh;

let logged = 0;

const main = () => {
  aels();
  onResize();
};

const aels = () => {
  O.ael('resize', onResize);
};

const onResize = evt => {
  iw = O.iw;
  ih = O.ih;
  iwh = iw / 2;
  ihh = ih / 2;
  
  canvas.width = iw;
  canvas.height = ih;
  
  g.fillStyle = 'white';
  g.fillRect(0, 0, iw, ih);
  
  g.strokeStyle = 'black';
  g.lineWidth = 1;
  
  render();
};

const render = () => {
  g.fillStyle = 'white';
  g.fillRect(0, 0, iw, ih);
  
  const s = 300;
  
  g.translate(round(iwh) + .5, round(ihh) + .5);
  g.scale(s, s);
  g.translate(-.5, -.5);
  
  g.lineWidth = 1 / s;
  g.beginPath();
  g.rect(0, 0, 1, 1);
  g.stroke();
  
  const cs = [
    462, 1024,
    462, 1024 - 165,
    462, 1024 - 48,
    462 + 92, 1024 - 48 - 42,
  ].map(a => a / 1024);
  
  log(...cs);
  
  const [
    ax, ay,
    bx, by,
    cx, cy,
    dx, dy,
  ] = cs;
  
  g.beginPath();
  g.moveTo(ax, ay);
  g.lineTo(bx, by);
  g.stroke();
  
  g.beginPath();
  g.moveTo(cx, cy);
  g.lineTo(dx, dy);
  g.stroke();
  
  const [x1, y1, angle, scale] = calc(ax, ay, bx, by, cx, cy, dx, dy);
  const x2 = x1 + scale * cos(angle);
  const y2 = y1 + scale * sin(angle);
  const x3 = x2 + scale * cos(angle + pih);
  const y3 = y2 + scale * sin(angle + pih);
  const x4 = x3 + scale * cos(angle + pi);
  const y4 = y3 + scale * sin(angle + pi);
  
  if(!logged){
    log(x1, y1, x2, y2, x4, y4);
    logged = 1;
  }
  
  g.beginPath();
  g.moveTo(x1, y1);
  g.lineTo(x2, y2);
  g.lineTo(x3, y3);
  g.lineTo(x4, y4);
  g.closePath();
  g.stroke();
  
  g.resetTransform();
};

const calc = (ax, ay, bx, by, cx, cy, dx, dy) => {
  const len1 = O.dist(ax, ay, bx, by);
  const len2 = O.dist(cx, cy, dx, dy);
  const s = len2 / len1;
  
  const phi1 = atan2(by - ay, bx - ax);
  const phi2 = atan2(dy - cy, dx - cx)
  const angle = phi2 - phi1;

  const p = atan2(ay, ax);
  const d = O.hypot(ax, ay) * s;
  const tx = cx - d * cos(angle + p);
  const ty = cy - d * sin(angle + p);
  
  return [tx, ty, angle, s];
};

const cmp = (a, b) => {
  if(abs(a - b) < 1e-5) return;
  
  log(a);
  log(b);
  
  assert.fail();
};

main();