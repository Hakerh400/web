"use strict"

const assert = require("assert")

const main = () => {
  const doc = O.doc
  const html = doc.documentElement
  const body = doc.body
  
  doc.title = "about:blank"
  
  for(const elem of [html, body])
    elem.style.setProperty("background", "white")
}

main()