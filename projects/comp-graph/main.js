"use strict"

const fs = require("fs")
const path = require("path")
const assert = require("../assert")
const cp = require("child_process")
const O = require("../js-util")

const comp_steps_num = 1e3

const {K, S} = O.enum(["K", "S"])

const col_aux = new Uint8ClampedArray(3);

const main = function*(){
  const w = 100
  const h = 100
  
  // log((yield [ser_expr, yield [parse_expr, `
  //   S (S K K) (S K K) (S (S K K) (S K K))
  // `]]).toString())
  // return

  // {
  //   const s0 = `
  //     S (S K K) (S K K) (S (S K K) (S K K))
  //   `
  // 
  //   /*|
  //     | S (S K K) --- +2
  //     | S         --- 2,0 2,1
  //     |    S K K  --- +2
  //     |    S K    --- 2,1
  //     |    S      --- 2,0 2,1
  //     |      K    --- 2,0 2,0
  //     |        K  --- +0
  //     |
  //     | ((1 * 2 * 2 + 1 + 2) * 2 + 1) * 2 + 2
  //     |*/
  // 
  //   const e1 = yield [parse_expr, s0]
  //   const n1 = yield [ser_expr, e1]
  //   const e2 = yield [dser_expr, n1]
  //   const n2 = yield [ser_expr, e2]
  // 
  //   const s1 = yield [show_expr, e1]
  //   const s2 = yield [show_expr, e2]
  // 
  //   log(s1)
  //   log(s2)
  //   log(n1.toString())
  //   log(n2.toString())
  // 
  //   return
  // }

  const ofy = 15132045490n
  const ofx = 0n

  const canvas = O.ce(O.body, "canvas")
  
  canvas.width = w
  canvas.height = h

  const g = canvas.getContext("2d")
  const imgd = new O.ImageData(g, 1)

  imgd.iter((x, y) => {
    return O.rec(function*(){
      const e1 = yield [dser_expr, ofy + BigInt(y)]
      const e2 = yield [dser_expr, ofx + BigInt(x)]
      const e0 = mk_app(e1, e2)
      const ref = [comp_steps_num]
      const e = yield [reduce_whnf_n, e0, ref]
      const [comb] = yield [split_expr, e]
      const [n] = ref
      const k = n / comp_steps_num

      if(comb === K){
        col_aux[0] = 0
        col_aux[1] = 255
        col_aux[2] = 0
      }else{
        col_aux[0] = 255
        col_aux[1] = 0
        col_aux[2] = 0
      }

      for(let i = 0; i !== 3; i++)
        col_aux[i] *= k

      return col_aux
    })
  })

  imgd.put()
}

const is_whnf = e => {
  if(is_comb(e)) return 1

  const e1 = e[0]
  if(is_comb(e1)) return 1

  return e1[0] === S
}

const reduce_whnf_1_aux = function*(e){
  const [target, xs] = yield [split_expr, e]

  if(target === K){
    const [a, b, ...xs1] = xs
    return O.tco(list_to_expr, [a, ...xs1])
  }

  if(target === S){
    const [a, b, c, ...xs1] = xs
    const e1 = mk_app(a, c)
    const e2 = mk_app(b, c)
    const e = mk_app(e1, e2)

    return O.tco(list_to_expr, [e, ...xs1])
  }

  assert.fail(target)
}

const reduce_whnf_1 = function*(e){
  if(is_whnf(e)) return e
  return O.tco(reduce_whnf_1_aux, e)
}

const reduce_whnf_n = function*(e, ref){
  let [n] = ref

  assert(n >= 0)
  assert(n === (n | 0))

  while(n--){
    if(is_whnf(e)) break
    e = yield [reduce_whnf_1_aux, e]
  }

  ref[0] = n
  return e
}

const mk_app = (a, b) => {
  return [a, b]
}

const list_to_expr = function*(xs){
  if(xs === K || xs === S) return xs

  const n = xs.length
  assert(n !== 0)

  let e = xs[0]

  for(let i = 1; i !== n; i++){
    const arg = yield [list_to_expr, xs[i]]
    e = mk_app(e, arg)
  }

  return e
}

const expr_to_list = function*(e){
  if(is_comb(e)) return [e]
  
  const [a, b] = e
  const xs = yield [expr_to_list, a]

  xs.push(b)

  return xs
}

const split_expr = function*(e){
  const xs = yield [expr_to_list, e]
  const target = xs.shift()

  return [target, xs]
}

const parse_expr_aux = function*(toks){
  let e = null

  const push = e1 => {
    if(e === null){
      e = e1
      return
    }

    e = mk_app(e, e1)
  }

  while(toks.length !== 0){
    const tok = toks.shift()

    if(tok === "K"){
      push(K)
      continue
    }

    if(tok === "S"){
      push(S)
      continue
    }

    if(tok === "("){
      const e1 = yield [parse_expr_aux, toks]
      assert(toks.shift() === ")")
      push(e1)
      continue
    }

    if(tok === ")"){
      toks.unshift(tok)
      break
    }

    assert.fail(tok)
  }

  assert(e !== null)

  return e
}

const parse_expr = function*(s){
  const toks = O.match(s.trimStart(), /[KS\(\)]\s*|./g)
    .map(a => a.trimEnd())
  
  return O.tco(parse_expr_aux, toks)
}

const show_expr = function*(e, parens=0){
  if(e === K) return "K"
  if(e === S) return "S"

  const n = e.length
  assert(n === 2)

  const [a, b] = e
  const s1 = yield [show_expr, a, 0]
  const s2 = yield [show_expr, b, 1]

  let s = `${s1} ${s2}`
  if(parens) s = `(${s})`

  return s
}

const is_comb = e => {
  return e === K || e === S
}

const is_app = e => {
  return !is_comb(e)
}

const ser_expr_aux = function*(ser, e, last){
  if(last){
    if(is_comb(e)){
      ser.inc(e === K ? 0n : 1n)
      return
    }

    ser.inc(2n)

    const [a, b] = e

    yield [ser_expr_aux, ser, a, 0]
    yield [ser_expr_aux, ser, b, 1]

    return
  }

  if(is_comb(e)){
    ser.write(2n, 0n)
    ser.write(2n, e === K ? 0n : 1n)
    return
  }

  ser.write(2n, 1n)

  const [a, b] = e

  yield [ser_expr_aux, ser, a, 0]
  yield [ser_expr_aux, ser, b, 0]
}

const ser_expr = function*(e){
  const ser = new O.NatSerializer()

  yield [ser_expr_aux, ser, e, 1]

  return ser.output
}

const dser_expr_aux = function*(ser, last){
  if(last){
    if(ser.input < 2n){
      if(ser.input === 0n) return K
      return S
    }

    ser.input -= 2n

    const a = yield [dser_expr_aux, ser, 0]
    const b = yield [dser_expr_aux, ser, 1]

    return mk_app(a, b)
  }

  if(ser.read(2n) === 0n){
    if(ser.read(2n) === 0n) return K
    return S
  }

  const a = yield [dser_expr_aux, ser, 0]
  const b = yield [dser_expr_aux, ser, 0]

  return mk_app(a, b)
}

const dser_expr = function*(n){
  const ser = new O.NatSerializer(n)

  return O.tco(dser_expr_aux, ser, 1)
}

O.rec(main)