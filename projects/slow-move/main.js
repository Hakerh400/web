'use strict';

const assert = require('assert');

const cols = {
  bg: '#fff',
  line: '#000',
};

const {g} = O.ceCanvas();
const {canvas} = g;

let iw, ih;
let iwh, ihh;

const main = () => {
  aels();
  on_resize();
  render();
};

const aels = () => {
  O.ael('resize', on_resize);
};

const on_resize = evt => {
  iw = O.iw;
  ih = O.ih;
  iwh = iw / 2;
  ihh = ih / 2;
  
  canvas.width = iw;
  canvas.height = ih;
  
  g.lineCap = 'round';
  g.lineJoin = 'round';
};

const render = () => {
  g.fillStyle = cols.bg;
  g.fillRect(0, 0, iw, ih);
  
  const ofs = 100;
  const s = 100;
  const x = O.now / 1e7 % 1 * iw;
  
  g.strokeStyle = cols.line;
  g.lineWidth = 2;
  g.beginPath();
  g.moveTo(x, ofs);
  g.lineTo(x, ofs + s);
  g.stroke();
  
  O.raf(render);
};

main();