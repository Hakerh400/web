'use strict';

const assert = require('assert');
const modal = require('.');

const main = () => {
  let open = 0;

  O.body.style.margin = '0px';
  modal.div.innerText = 'abc';

  O.ael('keydown', evt => {
    if(evt.code !== 'Enter') return;
    
    if(open ^= 1) modal.open();
    else modal.close();
  });
};

main();