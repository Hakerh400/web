'use strict';

const SOUND = 0;

const {g} = O.ceCanvas();

let t0 = null;
let t = null;
let start = null;
let stage = 0;

let w = null;
let h = null;
let wh = null;
let hh = null;
let xOffset = null;

const main = () => {
  t0 = parseTime(O.urlParam('t', '00:05:00'));
  t = t0;
  
  O.ael('resize', onResize);
  O.ael('keydown', onKeyDown);
  
  onResize();

  O.raf(render);
};

const onResize = evt => {
  const {iw, ih} = O;
  
  w = iw;
  h = ih;
  wh = w / 2;
  hh = h / 2;
  
  const {canvas} = g;
  canvas.width = w;
  canvas.height = h;
  
  g.font = `${h * .4}px arial`;
  g.textBaseline = 'middle';
  g.textAlign = 'left';
  g.fillStyle = '#000';
  
  xOffset = g.measureText(formatTime(0)).width / 2;
};

const onKeyDown = evt => {
  if(evt.code !== 'Space') return;
  setTimeout(() => {
    start = O.now;
    stage = 1;
  }, 1e3);
};

const render = () => {
  const dt = t - (O.now - start) / 1e3;

  if(stage === 1 && dt < 0){
    if(SOUND){
      const a = new SpeechSynthesisUtterance('a');
      a.voice = speechSynthesis.getVoices()[21];
      speechSynthesis.speak(a);
    }
    
    stage = 2;
  }

  const tt = start !== null ? Math.max(dt, 0) : t;

  g.clearRect(0, 0, w, h);
  g.fillText(formatTime(tt), wh - xOffset, hh);
  
  O.raf(render);
};

const parseTime = str => {
  return str.match(/\d+/g).reduce((t, a, b) => {
    return t + (a | 0) * 60 ** (2 - b);
  }, 0);
};

const formatTime = time => {
  time = t0 - time;
  
  const h = time / 3600 | 0;
  const m = time / 60 % 60 | 0;
  const s = time % 60 | 0;

  return [h, m, s].map(a => String(a).padStart(2, '0')).join(':');
};

speechSynthesis.onvoiceschanged = main;