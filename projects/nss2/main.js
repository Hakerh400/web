'use strict';

const assert = require('assert');

const w = 6;
const h = 5;
const size = 60;
const speed = 1e3;

const cols = {
  canvas_bg: 'darkgray',
  text: '#000',
  bg: '#fff',
  s_alive: '#f80',
  s_dead: '#888',
  flag: '#f28',
  stone: '#a40',
  cbelt: '#0f0',
};

const tile_attribs = [
  's_alive',
  's_dead',
  'flag',
  'stone',
  'cbelt',
];

const nav_info = [
  ['up', [0, -1]],
  ['right', [1, 0]],
  ['down', [0, 1]],
  ['left', [-1, 0]],
];

const dflt_moves_num = 5;
const flag_moves_num = 8;

const {g} = O.ceCanvas(1);
const {canvas} = g;

let iw, ih;
let iwh, ihh;

let grid = null;
let solving = 0;

const main = () => {
  grid = new Grid(w, h, (x, y) => {
    return new Tile(x, y);
  });
  
  for(let x = 0; x !== w; x++)
    grid.get(x, 0).cbelt = 1;
  
  grid.get(2, 1).s_dead = 1;
  grid.get(5, 1).flag = 1;
  grid.get(5, 2).stone = 1;
  
  grid.set_player(5, 0);
  grid.set_player(5, 1);
  
  grid.update_moves_num();
  
  const buf0 = grid.save();
  
  const reset = () => {
    grid.load(buf0);
    render();
  };
  
  O.ael('resize', on_resize);
  
  O.ael('keydown', evt => {
    const {code} = evt;
    
    if(code === 'KeyR'){
      reset();
      return;
    }
    
    check_nav: {
      if(solving !== 0) break check_nav;
      
      const s = 'Arrow';
      if(!code.startsWith(s)) break check_nav;
      
      const dir_name = code.slice(s.length).toLowerCase();
      if(!grid.nav_by_dir_name(dir_name)) return;
      
      render();
      
      return;
    }
    
    if(code === 'KeyS'){
      reset();
      solve().catch(log);
      return;
    }
    
    if(code === 'Escape'){
      if(solving === 1) solving = 2;
      return;
    }
  });
  
  on_resize();
};

class Grid extends O.Grid {
  constructor(w, h){
    super(w, h, (x, y) => {
      return new Tile(x, y);
    });
    
    this.w = w;
    this.h = h;
    this.w1 = w - 1;
    this.h1 = h - 1;
    
    this.buf_size = w * h + 1;
    this.init();
  }
  
  init(){
    this.moves_num = dflt_moves_num;
  }
  
  clear(){
    this.init();
    
    this.iter((x, y, d) => {
      d.clear();
    });
  }
  
  update_moves_num(){
    this.iter((x, y, d) => {
      if(!d.flag) return;
      if(!d.player) return;
      
      this.moves_num = flag_moves_num;
      
      if(d.s_dead){
        d.s_dead = 0;
        d.s_alive = 1;
      }
    });
  }
  
  set_player(x, y, alive=1){
    const d = this.get(x, y);
    d.set_player(alive);
  }
  
  nav(dir){
    if(this.moves_num === 0) return 0;
    
    const {w, w1} = this;
    const [dx, dy] = nav_info[dir][1];
    const buf = this.save();
    
    let moved = 0;
    let exited = 0;
    
    this.iter((x, y, d) => {
      d.moved = 0;
    });
    
    this.iter((x, y, d) => {
      if(d.moved) return;
      if(!d.s_alive) return;
      
      stage_0: {
        let s_alive = 1;
        let s_dead = 0;
        let stone = 0;
        let flag = 0;
        
        let x1 = x;
        let y1 = y;
        
        while(1){
          x1 += dx;
          y1 += dy;
          
          const d1 = this.get(x1, y1);
          
          if(d1 === null){
            if(x1 === w && (s_alive || s_dead))
              exited = 1;
            
            return;
          }
          
          if(d1.is_free(stone)) break;
          
          s_alive = d1.s_alive;
          s_dead = d1.s_dead;
          stone = d1.stone;
          flag = d1.flag;
        }
      }
      
      stage_1: {
        let s_alive_prev = 0;
        let s_dead_prev = 0;
        let stone_prev = 0;
        let flag_prev = 0;
        
        let s_alive = 1;
        let s_dead = 0;
        let stone = 0;
        let flag = 0;
        
        let x1 = x;
        let y1 = y;
        let d_prev = d;
        
        while(1){
          x1 += dx;
          y1 += dy;
          
          const d1 = this.get(x1, y1);
          if(d1 === null) assert.fail();
          
          let is_free = d1.is_free(stone);
          
          const s_alive1 = d1.s_alive;
          const s_dead1 = d1.s_dead;
          const stone1 = d1.stone;
          const flag1 = d1.flag;
          
          if(s_alive){
            d_prev.s_alive = s_alive_prev;
            d1.s_alive = 1;
            d1.moved = 1;
          }
          
          if(s_dead){
            d_prev.s_dead = s_dead_prev;
            d1.s_dead = 1;
          }
          
          if(stone){
            d_prev.stone = stone_prev;
            d1.stone = 1;
          }
          
          if(flag && stone_prev){
            d_prev.flag = flag_prev;
            d1.flag = 1;
          }
          
          if(is_free) break;
          
          d_prev = d1;
          s_alive_prev = s_alive;
          s_dead_prev = s_dead;
          stone_prev = stone;
          flag_prev = flag;
          
          s_alive = s_alive1;
          s_dead = s_dead1;
          stone = stone1;
          flag = flag1;
        }
      }
      
      moved = 1;
    });
    
    if(this.get(w1, 0).player) exited = 1;
    
    if(!moved || exited){
      this.load(buf);
      return 0;
    }
    
    this.update_moves_num();
    this.moves_num--;
    
    if(this.moves_num === 0){
      this.iter((x, y, d) => {
        if(!d.s_alive) return;
        
        d.s_alive = 0;
        d.s_dead = 1;
      });
    }
    
    for(let x = w1; x >= 0; x--){
      const d = this.get(x, 0);
      const d1 = this.get(x - 1, 0);
      
      if(d1 === null){
        d.s_alive = 0;
        d.s_dead = 0;
        d.s_stone = 0;
        break;
      }
      
      d.s_alive = d1.s_alive;
      d.s_dead = d1.s_dead;
      d.s_stone = d1.s_stone;
    }
    
    this.update_moves_num();
    
    return 1;
  }
  
  nav_by_dir_name(dir_name){
    const dir = nav_info.findIndex(s => s[0] === dir_name);
    assert(dir !== -1);
    
    return this.nav(dir);
  };
  
  save(){
    const {buf_size} = this;
    const buf = O.Buffer.alloc(buf_size);
    
    let i = 1;
    
    buf[0] = this.moves_num;
    
    this.iter((x, y, d) => {
      buf[i++] = d.save();
    });
    
    return buf;
  }
  
  load(buf){
    this.clear();
    this.moves_num = buf[0];
    
    let i = 1;
    
    this.iter((x, y, d) => {
      d.load(buf[i++]);
    });
  }
  
  load_from_str(s){
    const buf = O.Buffer.from(s, 'hex');
    this.load(buf);
  }
  
  to_str(){
    const buf = this.save();
    return buf.toString('hex');
  }
}

class Tile {
  static attribs = tile_attribs;
  static attribs_rev = [...Tile.attribs].reverse();
  
  constructor(x, y){
    this.x = x;
    this.y = y;
    
    this.clear();
  }
  
  clear(){
    const attribs = this.get_attribs();
    
    for(const name of attribs)
      this[name] = 0;
  }
  
  get_attribs(rev=0){
    if(rev) return Tile.attribs_rev;
    return Tile.attribs;
  }
  
  get player(){
    return this.s_alive || this.s_dead;
  }
  
  is_free(for_stone=0){
    if(this.player) return 0;
    if(this.stone) return 0;
    if(for_stone && this.flag) return 0;
    return 1;
  }
  
  set_player(alive=1){
    this.s_alive = alive;
    this.s_dead = !alive;
    this.stone = 0;
  }
  
  save(){
    const attribs = this.get_attribs(1);
    
    return attribs.reduce((n, name) => {
      return (n << 1) | this[name];
    }, 0);
  }
  
  load(n){
    const attribs = this.get_attribs();
    
    for(const name of attribs){
      if(n & 1) this[name] = 1;
      n >>= 1;
    }
  }
  
  to_str(){
    return this.save().toString();
  }
  
  toString(){
    return this.to_str();
  }
}

const solve = async () => {
  if(solving !== 0) return;
  solving = 1;
  
  const buf0 = grid.save();
  const queue = [['', buf0]];
  const states = new Set([buf0.toString('hex')]);
  
  let sp = 0;
  let total_states = 1;
  
  main_loop: while(queue.length !== 0){
    if(solving === 2){
      break main_loop;
    }
    
    if(++sp === speed){
      sp = 0;
      render();
      await O.rafa(O.nop);
    }
    
    const [moves, buf] = queue.shift();
    
    grid.load(buf);
    
    for(let dir = 0; dir !== 4; dir++){
      if(!grid.nav(dir)) continue;
      
      const moves1 = moves + dir;
      
      const cnd = (
        grid.get(5, 0).s_alive &&
        grid.get(5, 1).s_alive &&
        grid.get(5, 3).s_alive
      );
      
      if(cnd){
        log(moves1);
        break main_loop;
      }
      
      const buf1 = grid.save();
      const state = buf1.toString('hex');
      
      if(!states.has(state)){
        states.add(state);
        queue.push([moves1, buf1]);
        total_states++;
      }
      
      if(dir !== 3) grid.load(buf);
    }
  }
  
  render();
  solving = 0;
  
  // log(total_states);
};

const on_resize = evt => {
  iw = O.iw;
  ih = O.ih;
  iwh = iw / 2;
  ihh = ih / 2;
  
  canvas.width = iw;
  canvas.height = ih;
  
  g.textAlign = 'center';
  g.textBaseline = 'middle';
  g.font(32);
  
  render();
};

const render = () => {
  g.fillStyle = cols.canvas_bg;
  g.fillRect(0, 0, iw, ih);
  
  g.translate(iwh, ihh);
  g.scale(size);
  g.translate(-w / 2, -h / 2);
  
  const {gs} = g;
  
  grid.iter((x, y, d) => {
    g.save();
    g.translate(x, y);
    
    g.fillStyle = d.cbelt ? cols.cbelt : cols.bg;
    g.fillRect(0, 0, 1, 1);
    
    if(d.flag){
      g.fillStyle = cols.flag;
      g.beginPath();
      g.rect(.1, .1, .8, .8);
      g.fill();
      g.stroke();
    }
    
    if(d.s_alive || d.s_dead){
      g.fillStyle = d.s_alive ? cols.s_alive : cols.s_dead;
      g.beginPath();
      g.arc(.5, .5, .3, 0, O.pi2);
      g.fill();
      g.stroke();
    }
    
    if(d.stone){
      g.fillStyle = cols.stone;
      g.beginPath();
      g.rect(.2, .2, .6, .6);
      g.fill();
      g.stroke();
    }
    
    g.restore();
  });
  
  g.fillStyle = cols.text;
  g.fillText(grid.moves_num, .5, -.5);
  
  g.beginPath();
  for(let y = 0; y <= h; y++){
    g.moveTo(0, y);
    g.lineTo(w + gs, y);
  }
  for(let x = 0; x <= w; x++){
    g.moveTo(x, 0);
    g.lineTo(x, h + gs);
  }
  g.stroke();
  
  g.resetTransform();
};

main();